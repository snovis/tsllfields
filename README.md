TSLLFIELDS
=========

This application handles fields reservations.  It is closely linked with tsllsched
for managing a baseball schedule.


Revisions
------------
v1.53 Fix email bug. 
v1.52 Make changes for 2013 Season  
v1.51  Created this document and fixed a bug with release fields.  


TSLL How to Start a New Season
==============================



1. Gather the new managers names, teams, and email addresses
2. Remove all the old managers
3. Create the season page for the current season (eg Spring 2013 Season)
4. Create team list page for the current season

How the Team List Works
-----------------------

You have to assign the team page a template.  The Template gets the team data type.
So how do we set the current season?

   Note: What a hack.  I just added another template for the current year. 
   The thing should really store the year somewhere and to it correctly... Suck.
   
I don't understand how the schedule is being attached to the team page.  Heck,
I'm not even sure how the team page is being displayed...
okay, the work is done in the template `single-tsllteam.php`

Copyright Notice:
-----------------
    This file is part of tsllfields.

    TSLLFields is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TSLLFields is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TSLLFields.  If not, see <http://www.gnu.org/licenses/>.
