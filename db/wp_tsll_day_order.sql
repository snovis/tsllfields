SET NAMES latin1;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `wp_tsll_day_order`;

CREATE TABLE `wp_tsll_day_order` (
  `id` int(11) NOT NULL auto_increment,
  `day` varchar(16) NOT NULL,
  `order` smallint(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


SET FOREIGN_KEY_CHECKS = 1;
