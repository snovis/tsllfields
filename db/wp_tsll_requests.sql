SET NAMES latin1;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `wp_tsll_requests`;

CREATE TABLE `wp_tsll_requests` (
  `id` int(11) NOT NULL auto_increment,
  `team_id` int(11) NOT NULL,
  `req1` datetime NOT NULL,
  `req2` datetime NOT NULL,
  `req3` datetime NOT NULL,
  `req4` datetime NOT NULL,
  `req5` datetime NOT NULL,
  `req6` datetime NOT NULL,
  `comments` varchar(1024) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `team_id` (`team_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

insert into `wp_tsll_requests` values('7','1','2010-02-15 17:30:00','2010-02-16 17:30:00','2010-02-18 17:30:00','1969-12-31 17:00:00','1969-12-31 17:00:00','1969-12-31 17:00:00','','2010-02-15 17:30:00','2010-02-15 17:30:00');

SET FOREIGN_KEY_CHECKS = 1;
