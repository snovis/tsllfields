SET NAMES latin1;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `wp_tsll_options`;

CREATE TABLE `wp_tsll_options` (
  `option_item` varchar(30) NOT NULL,
  `option_value` text NOT NULL,
  PRIMARY KEY  (`option_item`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert into `wp_tsll_options` values('tsllfields_version','1.1'),
 ('open_requests_date','2/15/2010'),
 ('open_requests_time','15:00:00'),
 ('close_requests_date','02/16/2010'),
 ('close_requests_time','23:30:00');

SET FOREIGN_KEY_CHECKS = 1;
