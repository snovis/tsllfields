SET NAMES latin1;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `wp_tsll_gametimes`;

CREATE TABLE `wp_tsll_gametimes` (
  `id` int(11) NOT NULL auto_increment,
  `day` enum('SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY') NOT NULL default 'SATURDAY',
  `start_time` time NOT NULL,
  `notes` varchar(1024) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

insert into `wp_tsll_gametimes` values('5','MONDAY','17:30:00','','2010-02-11 05:25:24','2010-02-11 05:25:24'),
 ('2','TUESDAY','17:30:00','23','2010-02-11 04:56:59','2010-02-11 05:55:33'),
 ('3','WEDNESDAY','13:30:00','srn','2010-02-11 04:58:07','2010-02-11 05:56:18'),
 ('4','TUESDAY','19:30:00','Minors / Farm A','2010-02-11 05:22:27','2010-02-11 14:36:42'),
 ('6','MONDAY','19:30:00','Farm A or Majors','2010-02-11 14:36:13','2010-02-11 14:36:13'),
 ('7','THURSDAY','17:30:00','Minors','2010-02-11 15:18:14','2010-02-11 15:18:14');

SET FOREIGN_KEY_CHECKS = 1;
