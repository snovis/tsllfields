SET NAMES latin1;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `wp_tsll_teams`;

CREATE TABLE `wp_tsll_teams` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL,
  `manager` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `division` enum('TBALL','FARM-A','FARM-N','MINORS','MAJORS','OTHER') NOT NULL default 'MINORS',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

insert into `wp_tsll_teams` values('1','Red Sox','Scott Novis','scottnovis@gmail.com','MINORS','2010-02-11 02:05:50','2010-02-11 02:05:50'),
 ('2','Braves','Charles Ross','crz@test.com','MINORS','2010-02-11 02:06:19','2010-02-11 05:27:24'),
 ('0','Open','Peter Ewan','pe@test.com','OTHER','2010-02-12 05:43:21','2010-02-12 05:43:23'),
 ('4','Yankees','Lou Farina','lf@test.com','MAJORS','2010-02-12 16:57:48','2010-02-12 16:57:48');

SET FOREIGN_KEY_CHECKS = 1;
