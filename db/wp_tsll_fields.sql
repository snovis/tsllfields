SET NAMES latin1;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `wp_tsll_fields`;

CREATE TABLE `wp_tsll_fields` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL,
  `description` varchar(64) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

insert into `wp_tsll_fields` values('1','TSC3','Tempe Sports Complex South West Field','2010-02-11 02:08:37','2010-02-11 15:33:59'),
 ('2','TSC4','Tempe Sports Complex South East Field','2010-02-11 02:35:26','2010-02-11 05:33:46'),
 ('3','TSC6','Tempe Sports Complex North West Field','2010-02-11 15:33:29','2010-02-11 15:33:47'),
 ('4','TSC7','Tempe Sports Complex North East Field','2010-02-11 15:33:38','2010-02-11 15:33:38');

SET FOREIGN_KEY_CHECKS = 1;
