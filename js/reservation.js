/**
 *  Copyright (C) 2010 Scott Novis <scott@novisware.com>
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms 
 *  of the GNU General Public License as published by the Free Software Foundation, version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along 
 *  with this program. 
 *  
 *  If not, see <http://www.gnu.org/licenses/>.
 **/
 
// Trap console if it's not defined.
if (typeof console == "undefined") {
    window.console = {
        log: function () {}
    };
}

$j = jQuery.noConflict();


function ajaxRequestCallback(data) {
    var response =data;
    if (response.success) {
        $j('#tsll_table_div').html("");
        $j('#button_bar').hide();
        $j('#tsll_instructions').html("");
        $j('#tsll_results').html(response.html);
        $j('#legend_div').html('');
        $j(function(){
            var count = 10;
            countdown = setInterval(function(){
              $j("#legend_div").html("Redirecting in " + count + " seconds.");
              if (count == 0) {
                window.location = response.targetURL;
              }
              count--;
            }, 1000);
        });
    } else {
        $j('#tsll_dialog').html("<p>Request Can Not be processed.</p><span class='error'>" + response.html+"</span")
            .dialog('option','title','Request Error')
            .dialog('open');        
    }

}
/**
 * User is ready to request their fields.
 */
function postFieldRequest(slot_id) {
    /*
     * We now have a list of requests
     */
    var paramBlock = {
        action : 'tsll_field_request',
        tsll_action : 'request',
        team_id : $j('#team_name').attr('team_id'), // need a team id
        request : slot_id
    };
    $j.post(ajaxurl,paramBlock,ajaxRequestCallback,"json");

}

/**
 * System is hopefully calling us back with all sorts of useful goodness.
 */
function ajaxStartRequestCallback(data,o,x) {
    $ = $j;
    var response = data; //eval("("+data+")");
    if (response.success) {
        $('#login_block').hide();
        $('#tsll_results').html(response.html);
        $('#tsll_table_div').html(response.table);
        // Since this is the module that has to know about it, let this module put the data there
        $('#team_name').attr('team_id',response.team_id);
        $('#submit_request').click(postFieldRequest);
        $('#tsll_instructions').html(
            "<p>To reserve a field click on the box in the table for the time and date and field that you want.  "
            + "<p>All first requests for a week will be granted.  If you request a field in a week that is under "
            + "<b>first come first serve</b> reservation rules then you will get a field if it is available.  "
            + " Otherwise second requests will be denied until protected window closes to give everyone a chance to request a field. "
            + "</p> "
            + "<p>You will receive <b>an email</b> as soon as the request process is run letting you know if the system was able "
            + "to grant you a field.</p>"
        );
        $('TD.OPEN').click(requestField).css('color','green');
        
        var dialogOptions = {
            autoOpen: false,
            resizable: false,
            buttons : {
                //Cancel: function () { $(this).dialog("close") },
                "Ok" : function () { $(this).dialog("close") }
            }
        };
        $('#tsll_dialog').dialog(dialogOptions);
        
    } else {
        $('#tsll_results').html(response.html);
    }
    
}
/**
 * Okay user clicked the reservation button, now he/she wants to
 * reserve a field.  So we ask tsllfields plugin to render a "requestable"
 *  version of the table.
 */
function ajaxStartRequest() {
    $ = $j;
    var params = {
            action : 'tsll_field_request',
            tsll_action : 'StartRequest',
            email: $('#email').val()
        };
            
    $.post(ajaxurl,params,ajaxStartRequestCallback,"json");
    return false; // don't send the form, we let the ajax handle it!   
}

function requestField(o) {
    var cell = $j(o.currentTarget);
    var title = cell.attr('title');
    yes = confirm("Request Field?\n"+title);
    if (yes) {
        
        var id = cell.attr('id');
        console.log(cell);
        postFieldRequest(id);
    }
    
}
$j(document).ready(function($) {
    $('#btn_submit').click(ajaxStartRequest);


});