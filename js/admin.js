/**
 *  Copyright (C) 2010 Scott Novis <scott@novisware.com>
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms 
 *  of the GNU General Public License as published by the Free Software Foundation, version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along 
 *  with this program. 
 *  
 *  If not, see <http://www.gnu.org/licenses/>.
 **/
 
/**
 * Javascript for admin panel functions.
 */
$j = jQuery.noConflict();

// Use a function as a jQuery extension
$j.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});

function submit_edit()
{
    $j('#batch_action').val('BatchEdit');
    $j('#reservation_table_form').submit();
}

/**
 * Accept the changes to a row
 */
function updateRecord(e) {
    var resv = {'id':"",
                'date':"",
                'day':"",
                'time':"",
                'field':"",
                'team':"",
                'division':""
                };
    var row = $j(this).parent().parent();
    var id_cell = row.children().first();
    resv.id = id_cell.text();
       
    date_cell = id_cell.next();
    resv.date = date_cell.text();
    
    day_cell = date_cell.next();
    resv.day = day_cell.text();
    
    time_cell = day_cell.next();
    resv.time = time_cell.find('select').val();
    
    field_cell = time_cell.next();
    resv.field = field_cell.find('select').val();
    
    team_cell = field_cell.next();
    resv.team = team_cell.find('select').val();
    
    division_cell = team_cell.next();
    resv.division = division_cell.text();
    
    var a = $j(this);
    var params = "&action=Accept";
    params += "&id="+resv.id;
    params += "&start_time="+resv.time;
    params += "&team_id="+resv.team;
    params += "&field_id="+resv.field;
    var href = tsll_request_path + params;
    a.attr('href',href);
    a.unbind(); // causes click to kick off which makes us jump to the link!
    //a.click();
}

/**
 * User hit cancel button, so just reload page
 */
function abort(e) {
    var a = $j(this);
    var href = tsll_request_path;
    a.attr('href',href);
    a.unbind(); // causes click to kick off which makes us jump to the link!   
}

/**
 * allow user to edit a reservation in the row
 */
function editRow(e) {
    var resv = {'id':"",
                'date':"",
                'day':"",
                'time':"",
                'field':"",
                'team':"",
                'division':""
                };
    var row = $j(this).parent();
    var id_cell = $j(this);
    resv.id = id_cell.text();
    id_cell.removeClass('resv_id').addClass('unlocked');
    
    date_cell = id_cell.next();
    resv.date = date_cell.text();
    
    day_cell = date_cell.next();
    resv.day = day_cell.text();
    
    time_cell = day_cell.next();
    resv.time = time_cell.text();
    
    field_cell = time_cell.next();
    resv.field = field_cell.text();
    
    team_cell = field_cell.next();
    resv.team = team_cell.text();
    
    division_cell = team_cell.next();
    resv.division = division_cell.text();
  
    checkbox_cell = division_cell.next();
    edit_cell = checkbox_cell.next();
    cancel_cell = edit_cell.next();
    
    
  /**
   * Make a series of editable fields.
   */
  var start_time_select = $j('select#start_time_select').clone();
  start_time_select.attr('id',start_time_select.attr('id')+"_"+resv.id);
  start_time_select.val(time_cell.attr('index'));
  time_cell.html(start_time_select);
  
  var field_select = $j('select#field_select').clone();
  field_select.attr('id',field_select.attr('id')+"_"+resv.id);
  field_select.val(field_cell.attr('index'));
  field_cell.html(field_select);

  var team_select = $j('select#team_select').clone();
  team_select.attr('id',team_select.attr('id')+"_"+resv.id);
  team_select.val(team_cell.attr('index'));
  team_cell.html(team_select);
  
  checkbox_cell.children().hide();
  var accept_icon_path = tsll_src_path + "/accept.png";
  var cancel_icon_path = tsll_src_path + "/cancel.png";
  edit_cell.html('<a id="update_'+resv.id+'"><img src="'+accept_icon_path+'"/></a>');
  cancel_cell.html('<a id="cancel_'+resv.id+'"><img src="'+cancel_icon_path+'"/></a>');
  $j('a#update_'+resv.id).click(updateRecord);
  $j('a#cancel_'+resv.id).click(abort);

}

jQuery(document).ready(function($) {
    $('form#team_form').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            manager: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: {
                required: "Need Team Name",
                minlength: "At least 2 characters required"
            },
            manager: {
                required: "We need Managers Name",
                minlength: "At least enter initials!"
            },
            email: {
                required: "Need Managers Email address",
                email: "Need a valid email address!"
            }
        }
    });
    
    $('form#field_form').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            }

        },
        messages: {
            name: {
                required: "Need Field Name",
                minlength: "At least 2 characters required"
            }
        }
    });
        
    $('form#generate_form').validate({
        rules: {
            start_date: {
                required: true,
                date: true
            }

        },
        messages: {
            name: {
                required: "Must enter a valid date",
                date: "Has to be in format mm/dd/yyyy"
            }
        }
    });


    $('.hidable').click(function() {
        $(this).next().toggle('slow');
        $(this).find('.clue').toggle();
    }).next().hide();
    
    // Show the edit box if we are editing a field.
    if ($.getUrlVars()["action"]=="Edit") {
        if ($('#edit_form_container').length > 0) $('#edit_form_container').show();
    }

    /*
     {cssHeader : "sortableHeader",
                                    cssAsc : "sortableHeaderUp",
                                    cssDesc : "sortableHeaderDown"}
    */
    $('table#reservation_table').tablesorter({headers: {7: { sorter: false} } });
    $('table#reservation_table tbody tr:odd').addClass('odd');
    
    if ($('#tsll_datepicker').length>0)                 $('#tsll_datepicker').datepicker();
    if ($('#tsll_reservation_datepicker').length>0)     $('#tsll_reservation_datepicker').datepicker();
    if ($('#open_requests_date').length>0)              $('#open_requests_date').datepicker();
    if ($('#close_requests_date').length>0)             $('#close_requests_date').datepicker();   
    if ($('#display_week').length>0)                    $('#display_week').datepicker();   
    if ($('#report_week').length>0)                     $('#report_week').datepicker();
    
    if ($('table#reservation_table').length>0) {
        $('a#checkall').click(function(){
            $j('INPUT[ name="record_id[]" ]').attr('checked',true);
            return false;
        });
        $('a#uncheckall').click(function(){
            $('INPUT[ name="record_id[]"]').attr('checked',false);
            return false;
        });
        $('table#reservation_table td.resv_id').click(editRow);
    }
    
});