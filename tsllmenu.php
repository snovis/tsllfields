<?php
/*
Plugin Name: tsllmenu
Plugin URI: http://tempesouth.com/
Description: TSLL Menu Widget
Author: Scott Novis
Version: 1.1
Author URI: http://tempesouth.com/
*/

/**
 *  Copyright (C) 2010 Scott Novis <scott@novisware.com>
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by the Free Software Foundation, version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. 
 *  If not, see <http://www.gnu.org/licenses/>.
 **/

function tsllmenu_widget() {
    global $current_user;
    global $user_login;

    if (current_user_can('level_0')) {
        get_currentuserinfo();
        $url = get_bloginfo('url');
        print "<b style='color:#700'>".$current_user->user_firstname . " " . $current_user->user_lastname . "</b>\n<br/>";
        if (current_user_can('level_10')){
?>
            <a href="<?=$url?>/wp-admin/">Site Admin</a><br/>
<?php
        }

        
        wp_loginout( get_bloginfo('url') );
        
?>     
        <h3>Coaching Tools</h3>
        <ul>
            <li><a href="<?= $url ?>/reservations">Reservations</a></li>
            <li><a href="<?= $url ?>/release-reservation">Release Field</a></li>            
            <li><a href="<?= $url ?>/manager-information">Manager Information</a></li>
            <li><a href="<?= $url ?>/practice-planner">Practice Planner</a></li>
            <li><a href="<?= $url ?>/roster-manager/">Roster Manager</a></li>
            <li><a href="<?=$url?>/schedule">Schedule</a></li>
        </ul>
  
<?php
    } else {
?>
        <h3>Login</h3>
        <div style="border: 1px inset black; padding: 5px;">
            <form action="<?php echo get_option('home'); ?>/wp-login.php" method="post">
            
                <label for="log">User</label><br/>
                <input type="text" name="log" id="log" value="<?php echo esc_html(stripslashes($user_login), 1) ?>" size="20" />
                <label for="pwd">Password</label><br/>
                <input type="password" name="pwd" id="pwd" size="20" />
                <input type="submit" name="submit" value="Send" class="button" />
                <br/>
                <p>
                   <label for="rememberme"><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> Remember me</label>
                   <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
                </p>
            </form>
            <center>
                <small><a href="<?php echo get_option('home'); ?>/wp-login.php?action=lostpassword">Recover password</a></small>
            </center>
        </div>
<?php
    }

}
 
function init_tsllmenu(){
	register_sidebar_widget("tsllmenu", "tsllmenu_widget");     
}
 
add_action("plugins_loaded", "init_tsllmenu");

