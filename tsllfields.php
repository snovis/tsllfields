<?php
/*
Plugin Name: TSLL Practice Field Reservation Tool
Plugin URI: http://tempesouthbaseball.org
Description: Create a system for managing practice field reservations.
Version: 1.54
Author: Scott Novis
Author URI: http://tempsouth.com

3/2 put in 2013 season changes.
3/4/13 01:18:52 PM put in code to add FROM to mail() $headers.
2/8/14 Put in code to display summary on manage reservations page.
2/8/14 Fixed bug with prepare() $SQL (was not needed for a query with no parameters)


 *  Copyright (C) 2015 Scott Novis <scott@novisware.com>
 *
 *  TSLLFields free software: you can redistribute it and/or modify it under the terms 
 *  of the GNU General Public License as published by the Free Software Foundation, version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along 
 *  with this program. 
 *  
 *  If not, see <http://www.gnu.org/licenses/>.

*/


/**
 * There's some important logic embedded in the relationship between the
 * tables and this code.
 * Essentially here's the key decision:
 * - You have a list of fields.
 * - You establish what days and times are available ACROSS THE BOARD (irrespective of fields)
 * - Then you generate (admin function) a complete matrix of all dates and times across
 *   all fields.
 * - Then you block out (reserver) with standing reservations any times that are NOT available.
 * - THEN you allocated times and dates for practices.
 */

define('TSLL_PLUGIN_DIR','/wp-content/plugins/' . basename(dirname(__FILE__)));
define('TSLL_TEAMS_TABLE', $table_prefix . 'tsll_teams');
define('TSLL_FIELDS_TABLE', $table_prefix . 'tsll_fields');
define('TSLL_OPTIONS_TABLE', $table_prefix . 'tsll_options');
define('TSLL_GAMETIMES_TABLE', $table_prefix . 'tsll_gametimes');
define('TSLL_RESERVATIONS_TABLE', $table_prefix . 'tsll_reservations');
define('TSLL_REQUESTS_TABLE', $table_prefix . 'tsll_requests');
define('TSLL_GAMES_TABLE',$table_prefix . 'tsll_games');
define('TSLLFIELDS_URI',get_bloginfo('wpurl').TSLL_PLUGIN_DIR);
define('OPEN_TEAM',1);   // special open team.  This id is reserved.
define('DATE_MYSQL','Y-m-d');
define('DATE_HUMAN','m/d/Y');
define('RULE_CLOSED',0);
define('RULE_PROTECTED',1);
define('RULE_FCFS',2);


date_default_timezone_set('America/Phoenix');
/**
 * delcare the class if it doesn't exist
 */
if (!class_exists("tsllfields")) {
	class tsllfields {

        // Admin page hooks
        var $team_page;
        var $field_page;
        var $gametimes_page;
        var $slots_page;
        var $options_page;
        var $summary_page;
        var $requests_page;
        var $assign_page;

        // key unix time stamps
        var $today;
        var $now;
        var $this_week_start;
        var $this_week_end;
        var $next_week_start;
        var $next_week_end;
        var $resv_rule;
        var $human_dates;
        var $sql_dates;
        var $protect_dates;


	    function tsllfields() { // constructor
            $this->check_tsllfields_tables();  // make sure our tables are installed!
            $script_path = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/reservation.js';
            wp_register_script('tsll-reservation',$script_path,array('jquery'));

            /**
             * Setup the date constraints.
             */
            $this->today = time();
            $this->now = time();

            /*
             * get Monday for this week.
             */
            $day_delta = 1-date("w",$this->today);
            $this->this_week_start = strtotime("+$day_delta days",$this->today);
            $this->this_week_end = strtotime("+7 days",$this->this_week_start);
            $this->next_week_start = $this->this_week_end;
            $this->next_week_end = strtotime("+7 days",$this->next_week_start);
            $this->human_dates = array('ThisWeek'=>date( DATE_HUMAN, $this->this_week_start),
                                       'NextWeek'=>date( DATE_HUMAN, $this->next_week_start) );
            $this->sql_dates = array('ThisWeekStart'=>date( DATE_MYSQL, $this->this_week_start ),
                                     'ThisWeekEnd'=>date( DATE_MYSQL, $this->this_week_end),
                                     'NextWeekStart'=>date( DATE_MYSQL, $this->next_week_start),
                                     'NextWeekEnd'=>date( DATE_MYSQL, $this->next_week_end),
                                     'Prev'=>date( DATE_MYSQL, strtotime('-7 days',$this->this_week_start)),
                                     'Next'=>date( DATE_MYSQL, $this->next_week_start)
                                    );

            $this->setReservationRules();

        }

        /**
         * addContent
         * core of the application, this generates the content for the user.
         * there are two tags, {TSLLFIELDS} - which displays the reservation system
         * and {TSLLWEEK} which displays the field reservations for a specific week.
         *
         * {TSLLWEEK:YYYY-MM-DD} is the format for displaying a calendar date.
         *
         * Time To Add One More - Release a Field.
         *
         * @param $content {string} The page content - we scan for our key word
         * @return $content {string}    the HTML content to display.
         */
		function addContent($content = '')
        {

            global $wpdb;
            global $current_user;
            get_currentuserinfo();
            $matches = array();


			if (strpos($content,'{TSLLFIELDS}')) {

                $sql = "SELECT * FROM " . TSLL_TEAMS_TABLE . " WHERE email='{$current_user->user_email}'";
                $team_info = $wpdb->get_row($sql,ARRAY_A);
                if (empty($team_info)) {
					$html = file_get_contents(  ABSPATH . "wp-content/plugins/tsllfields/html/view_only.html");

                    $html = str_replace("{DATE_THISWEEK}",$this->human_dates['ThisWeek'],$html);
                    $html = str_replace("{DATE_NEXTWEEK}",$this->human_dates['NextWeek'],$html);
                    $html = str_replace("{THISWEEK}",$this->generateReservationTable(true,$this->sql_dates['ThisWeekStart']),$html);
                    $html = str_replace("{NEXTWEEK}",$this->generateReservationTable(true,$this->sql_dates['NextWeekStart']),$html);
                } else {


					$html = file_get_contents(  ABSPATH . "wp-content/plugins/tsllfields/html/req_form.html");
					$html = str_replace("{USER_NAME}",$team_info['manager'],$html);
					$html = str_replace("{TEAM_NAME}",$team_info['name'],$html);
					$html = str_replace("{DIVISION}",ucwords(strtolower($team_info['division'])),$html);

                    $html = str_replace("{RULES_THISWEEK}","First Come First Serve", $html);

                    switch ($this->resv_rule) {
                        case RULE_FCFS:
                        // next week reservations are not yet open.
                             $html = str_replace("{RULES_NEXTWEEK}","First Come First Serve",$html);
                            break;
                        case RULE_PROTECTED:
                            $html = str_replace("{RULES_NEXTWEEK}","Limit 1 Per Team until  "
                                    . $this->protect_dates['End_Label'],$html);
                            break;
                        case RULE_CLOSED:
                        // next week reservations are not yet open.
                            $html = str_replace("{RULES_NEXTWEEK}","Closed Until " . $this->protect_dates['Start_Label'] . ". " ,$html);
                            break;
                    }


                    $html = str_replace("{DATE_THISWEEK}",$this->human_dates['ThisWeek'],$html);
                    $html = str_replace("{DATE_NEXTWEEK}",$this->human_dates['NextWeek'],$html);

                    //$window = $this->generateWeekWindow($this_week);
                    $start = $this->sql_dates['ThisWeekStart'];
                    $end = $this->sql_dates['ThisWeekEnd'];
                    $sql = "SELECT count(*) FROM " . TSLL_RESERVATIONS_TABLE . " WHERE DATE(gamedate) >='{$start}'
                        AND DATE(gamedate) < '{$end}' AND team_id={$team_info['id']}";
                    $count = $wpdb->get_var($sql);
                    $html = str_replace("{COUNT}",$count,$html);

                    $html = str_replace("{THISWEEK}",$this->generateNewReservationTable(true,$start),$html);
                    $html = str_replace("{NEXTWEEK}",$this->generateNewReservationTable(true,$this->sql_dates['NextWeekStart']),$html);

				}

				$content = str_replace('{TSLLFIELDS}',$html,$content);
		    } else if (preg_match('/\{TSLLWEEK:(\d{4}-\d{2}-\d{2})\}/',$content,$matches)) {
                $date = $matches[1];
                $html_table = $this->generateReservationTable(true,$date);
                $content = str_replace($matches[0],$html_table,$content);
            } else if (strpos($content,'{TSLLRELEASE}')) {
                $release_button = $this->getParam('release_button',null);
                $cancel_button = $this->getParam('cancel_button',null);
                $action = $this->getParam('tsll_action',null);
                $resv_id = $this->getParam('reservation_id','0');
                $release_code = $this->getParam('release_code','');
                if (!empty($release_button)) {
                    /* user wants to release a reservation */
                    $key = substr(md5($resv_id),0,8);
                    if ($key == $release_code) {
                        // To release the field just set the team id back to OPEN_TEAM
                        $modified = $this->getParam('modified',date('Y-m-d G:i:s'));
                        $data  = array( 'team_id'=>OPEN_TEAM, 'modified'=>$modified);
                        $where = array('id'=>$resv_id);
                        $results = $wpdb->update( TSLL_RESERVATIONS_TABLE,$data,$where );
                        $html = "<p>Field Reservation Released</p>";
                    } else
                        $html = "<p>Invalid Release Code.&nbsp; Action canceled.</p><p>Return Home: <a href='".get_bloginfo('url')."'>Return</a></p>";
                } else  if (!empty($cancel_button)) {
                    $html = "<p>Invalid Release Code.  Action canceled.</p><p>Return Home: <a href='".get_bloginfo('url')."'>Return</a></p>";
                } else {
                    // user is trying to release the field.
                    $html = file_get_contents(  ABSPATH . "wp-content/plugins/tsllfields/html/release_form.html");
                    if (!empty($resv_id)) {
                        $resv = $wpdb->get_row("SELECT *,DATE_FORMAT(gamedate,'%a %b-%d At %h:%i %p') as resv_date FROM " . TSLL_RESERVATIONS_TABLE . " WHERE id=$resv_id ",ARRAY_A);
                        $team = $wpdb->get_row("SELECT * FROM " . TSLL_TEAMS_TABLE . " WHERE id=".$resv['team_id'],ARRAY_A);
                        $field = $wpdb->get_row("SELECT * FROM " . TSLL_FIELDS_TABLE . " WHERE id=".$resv['field_id'],ARRAY_A);
                        $html_msg = "<p>Are you sure you want to release reservation:</p>";
                        $html_msg .="<p class='note'>Field " . $field['name'] . " is reserved on ".$resv['resv_date']. " for the " . $team['name'] . "</p>";
                    } else {
                        $html_msg = "<p>Please enter your field reservation and release code</p>";
                    }

                    $html = str_replace('{STATUS}',$html_msg,$html);
                    $html = str_replace('{RESERVATION_ID}',$resv_id,$html);
                    $html = str_replace('{RELEASE_CODE}',$release_code,$html);
                }
                $content = str_replace('{TSLLRELEASE}',$html,$content);
            } /*
            else if (is_page('results')) {
                if (strpos($content,'{TSLLGAMES}')) {
                    $html = $this->generateResults();
                    $content = str_replace('{TSLLGAMES}',$html,$content);
                }

            }
              */

			return $content;
		}

        /**
         * This function is responsible for verifying that the database tables
         * we want exist.  If they don't... we create them.
         */
        function check_tsllfields_tables()
        {
            global $wpdb;  // pointer to the database object

            $new_install = false;
            $tsll_teams_table_exists = false;
            $tsll_fields_table_exists = false;
            $tsll_gametimes_table_exists = false;
            $tsll_options_table_exists = false;
            $version_number = false;

            // Determine the calendar version
            $tables = $wpdb->get_results("show tables;");
            foreach ( $tables as $table )  {
                foreach ( $table as $value ) {
                    if ( $value == TSLL_TEAMS_TABLE ) $tsll_teams_table_exists = true;

                    if ( $value == TSLL_OPTIONS_TABLE ) $tsll_options_table_exists = true;

                    if ( $value == TSLL_GAMETIMES_TABLE ) $tsll_gametimes_table_exists = true;

                    if ( $value == TSLL_FIELDS_TABLE ) $tsll_fields_table_exists = true;
                }
            }
            if ($tsll_options_table_exists)
                $version_number = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='tsllfields_version'");

            if ($tsll_teams_table_exists == false && !empty($table_prefix)) $new_install = true;

            /**
             * Put logic here to handle upgrades, etc  For now just install the table.
             * Not sure how to remove data if we deactive plugin...
             */
            if ( $new_install ) {
                $wpdb->show_errors();
                $sql = "CREATE TABLE " . TSLL_TEAMS_TABLE . " (
                                id INT(11) NOT NULL AUTO_INCREMENT ,
                                name VARCHAR(32) NOT NULL ,
                                manager VARCHAR(64) NOT NULL ,
                                email VARCHAR(64) NOT NULL ,
                                division enum('FARM-A','FARM-N','MINORS','MAJORS','LEAGUE') NOT NULL DEFAULT 'MAJORS' ,
                                created DATETIME ,
                                modified DATETIME,
                                PRIMARY KEY (id)
                        )";
                $wpdb->query($sql);

                $sql = "INSERT INTO " . TSLL_TEAMS_TABLE . "
                        (name,manager,email,division,created,modified)
                        VALUES
                         ('OPEN','TSLL','me@scottnovis.com','LEAGUE',NOW(),NOW()),
                         ('RETURNED','TSLL','lights@tempesouth.com','LEAGUE',NOW(),NOW()),
                         ('GAME','TSLL','majors@tempesouth.com','MAJORS',NOW(),NOW()),
                         ('GAME','TSLL','minors@tempesouth.com','MINORS',NOW(),NOW()),
                         ('GAME','TSLL','farma@tempesouth.com','FARM-A',NOW(),NOW()),
                         ('GAME','TSLL','farmn@tempesouth.com','FARM-N',NOW(),NOW())
                ";
                $wpdb->query($sql);

                $sql = "CREATE TABLE " . TSLL_GAMETIMES_TABLE . " (
                                id INT(11) NOT NULL AUTO_INCREMENT ,
                                day ENUM('SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY', 'FRIDAY','SATURDAY') NOT NULL DEFAULT 'SATURDAY',
                                start_time TIME NOT NULL,
                                notes VARCHAR(256) NOT NULL,
                                created DATETIME ,
                                modified DATETIME,
                                PRIMARY KEY (id)
                        )";

                $wpdb->query($sql);

                $sql =  "INSERT INTO ". TSLL_GAMETIMES_TABLE
                        . " VALUES('1','MONDAY','17:30:00','',NOW(),NOW()), "
                        . " ('2','MONDAY','19:30:00','',NOW(),NOW()), "
                        . " ('3','TUESDAY','17:30:00','',NOW(),NOW()), "
                        . " ('4','TUESDAY','19:30:00','',NOW(),NOW()), "
                        . " ('5','WEDNESDAY','17:30:00','',NOW(),NOW()), "
                        . " ('6','WEDNESDAY','19:30:00','',NOW(),NOW()), "
                        . " ('7','THURSDAY','17:30:00','',NOW(),NOW()), "
                        . " ('8','THURSDAY','19:30:00','',NOW(),NOW()), "
                        . " ('9','FRIDAY','17:30:00','',NOW(),NOW()), "
                        . " ('10','FRIDAY','19:30:00','',NOW(),NOW()), "
                        . " ('11','SATURDAY','08:30:00','',NOW(),NOW()), "
                        . " ('12','SATURDAY','10:30:00','',NOW(),NOW()), "
                        . " ('13','SATURDAY','12:30:00','',NOW(),NOW()), "
                        . " ('14','SATURDAY','14:30:00','',NOW(),NOW()), "
                        . " ('15','SATURDAY','17:30:00','',NOW(),NOW()), "
                        . " ('16','SATURDAY','19:30:00','',NOW(),NOW()) ";

                $wpdb->query($sql);

                $sql = "CREATE TABLE " . TSLL_FIELDS_TABLE . " (
                                id INT(11) NOT NULL AUTO_INCREMENT ,
                                name VARCHAR(32) NOT NULL ,
                                description VARCHAR(64) NULL ,
                                priority int(11) NULL,
                                created DATETIME ,
                                modified DATETIME,
                                PRIMARY KEY (id)
                        )";


                $wpdb->query($sql);

                $sql =  "INSERT INTO ". TSLL_FIELDS_TABLE
                        . " VALUES (1, 'TSC3', 'South West Field',1, NOW(), NOW()),
                                (2, 'TSC4', 'South East Field',2, NOW(), NOW()),
                                (3, 'TSC6', 'North West Field',3, NOW(), NOW()),
                                (4, 'TSC7', 'North East Field',4, NOW(), NOW())";


                $wpdb->query($sql);

                $sql = "CREATE TABLE " . TSLL_REQUESTS_TABLE . " (
                                id INT(11) NOT NULL AUTO_INCREMENT ,
                                team_id INT(11) NOT NULL,
                                req1 DATETIME NOT NULL,
                                req2 DATETIME NULL,
                                req3 DATETIME NULL,
                                req4 DATETIME NULL,
                                req5 DATETIME NULL,
                                req6 DATETIME NULL,
                                created DATETIME ,
                                modified DATETIME,
                                PRIMARY KEY (id),
                                KEY team_id (team_id)
                        )";

                $wpdb->query($sql);


                $sql = "CREATE TABLE " . TSLL_RESERVATIONS_TABLE . " (
                            id INT(11) NOT NULL AUTO_INCREMENT,
                            gamedate DATETIME NOT NULL,
                            field_id INT(16) NOT NULL,
                            team_id INT(11) NOT NULL,
                            created DATETIME NOT NULL,
                            modified DATETIME NOT NULL,
                            PRIMARY KEY  (id)
                        )";

                $wpdb->query($sql);

                $sql = "CREATE TABLE " . TSLL_OPTIONS_TABLE . " (
                                option_item VARCHAR(30) NOT NULL ,
                                option_value TEXT NOT NULL ,
                                PRIMARY KEY (option_item)
                        )";
                $wpdb->query($sql);

                $today = date('m/d/Y');
                $sql = "INSERT INTO ".TSLL_OPTIONS_TABLE." VALUES ('tsllfields_version', '1.2'), "
                    . "('open_requests_date','$today'), "
                    . "('open_requests_time','08:00:00'), "
                    . "('close_requests_date','$today'), "
                    . "('close_requests_time','23:30:00'), "
                    . "('display_week',$today), "
                    . "('report_week',$today) ";

                $wpdb->query($sql);

            }

        }

		/**
		  * Incude our custom CSS and javascript ONLY if this is on page 19 - I'm connecting this to a specific page
		  * There is probably a better way of doing this but for now, just use the specific page number.
		  *
		  */
		function addHeaderCode()
        {

            if (is_page('Reservations')) {

                /**
                 * Can't call wp_enqueue_script here it's too late, and where it does work (init)
                 * I can't find out what page I'm rendering.  How cool is that?  But this works.
                 */
		 		echo "<!-- tsll plugin-code -->\n";
                echo '<script type="text/javascript">var ajaxurl="' . admin_url('admin-ajax.php') . '";</script>' . PHP_EOL; // creates the variable ajaxurl in javascript.

                /**
                 * jQuery UI Elements
                 */
                $jquery_ui_core_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/ui/ui.core.js';
                $jquery_ui_dialog_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/ui/ui.dialog.js';
                $jquery_ui_theme_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/css/theme/ui.all.css';
                print "<link rel='stylesheet' type='text/css' href='$jquery_ui_theme_url' />" . PHP_EOL;
                print "<script src='$jquery_ui_core_url' type='text/javascript'></script>" . PHP_EOL;
                print "<script src='$jquery_ui_dialog_url' type='text/javascript'></script>" . PHP_EOL;

                $script_path= get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/reservation.js';
                echo '<script src="' . $script_path . '" type="text/javascript" ></script>' . PHP_EOL;
			}
            echo '<link type="text/css" rel="stylesheet" href="' . get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/css/style.css" />' . "\n";
		}

        /**
         * Include our scripts and css if its one of our menu pages.
         */
        function adminHeaders()
        {
            $page = $this->getParam('page');
            if (!empty($page)) {
                $css_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/css/admin.css';
                print "<link rel='stylesheet' type='text/css' href='$css_url' />\n";
                $css_tablesorter_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/css/theme/blue/style.css';
                print "<link rel='stylesheet' type='text/css' href='$css_tablesorter_url' />\n";

                $script_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/admin.js';
                $validate_script_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/jquery.validate.js';
                $tablesorter_script_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/jquery.tablesorter.min.js';
                print "<script src='$validate_script_url' type='text/javascript'></script>" . PHP_EOL;
                print "<script src='$tablesorter_script_url' type='text/javascript'></script>" . PHP_EOL;

				/**
				 * for jQuery UI stuff
				 */
				if ($page == 'tsll-reservations') {
					$jquery_ui_core_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/ui/ui.core.js';
					$jquery_ui_datepicker_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/js/ui/ui.datepicker.js';
					$jquery_ui_theme_url = get_bloginfo('wpurl') . TSLL_PLUGIN_DIR . '/css/theme/ui.all.css';
				    print "<link rel='stylesheet' type='text/css' href='$jquery_ui_theme_url' />" . PHP_EOL;
					print "<script src='$jquery_ui_core_url' type='text/javascript'></script>" . PHP_EOL;
					print "<script src='$jquery_ui_datepicker_url' type='text/javascript'></script>" . PHP_EOL;
				}
                print "<script src='$script_url' type='text/javascript'></script>";
            }

        }


        /**
         * This builds the menu
         */
        function addResvMenu()
        {
            if (function_exists('add_menu_page'))  {
                add_menu_page('TSLL Field Reservation','TSLLFields','manage_options',__FILE__,array(&$this, 'displayMenuPage'), TSLLFIELDS_URI . "/images/menu.gif");
                $this->summary_page = add_submenu_page(__FILE__,'Manage Teams','Summary','manage_options',__FILE__,array(&$this, 'displayMenuPage'));
                $this->team_page = add_submenu_page(__FILE__,'Manage Teams','Manage Teams','manage_options','mg-teams',array(&$this, 'displayMenuManageTeams'));
                $this->field_page = add_submenu_page(__FILE__,'Manage Fields','Manage Fields','manage_options','tsll-fields',array(&$this, 'displayMenuManageFields'));
                $this->gametimes_page = add_submenu_page(__FILE__,'Manage Game Times','Manage Game Times','manage_options','tsll-gametimes',array(&$this, 'displayMenuManageGameTimes'));
                $this->slots_page = add_submenu_page(__FILE__,'Manage Fields','Manage Reservations','manage_options','tsll-reservations',array(&$this, 'displayMenuManageReservations'));
                $this->requests_page = add_submenu_page(__FILE__,'Manage Requests','Manage Requests','manage_options','tsll-requests',array(&$this, 'displayMenuManageRequests'));
                $this->assign_page = add_submenu_page(__FILE__,'Review Team Reservations','Review Reservations','manage_options','tsll-review',array(&$this, 'displayMenuReviewReservations'));
                $this->options_page = add_submenu_page(__FILE__,'Manage Options','Manage Options','manage_options','tsll-options',array(&$this, 'displayMenuOptions'));
            }
        }

        function displayMenuPage()
        {
            $view = new stdClass();
            $view->request_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsllfields/tsllfields.php';
            // if we want navigation buttons's we're going to have to create them.
            //$view->table_html = $this->generateReservationTable(true);
            $view->table_html = $this->generateNewReservationTable(true);
            include_once('views/main/main.phtml');
        }


        /**
         * displayMenuAssignReservations
         * Review reservation assignements to teams.
         *
         * Okay, we talked through some
         */
        function displayMenuReviewReservations()
        {
            global $wpdb;
            $view = new stdClass();
            $view->request_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsll-review';
            $view_path = 'views/review/';


            print "<div class='wrap'><div id='icon-fields' class='icon32'></div><h2>Review Team Reservation Totals</h2></div><br/>";


            $week = $this->getParam('week',null);

            if ($week) {
                /* if week is defined save it.  That way we always come back to the right spot*/
                $wpdb->update( TSLL_OPTIONS_TABLE,
                        array('option_value'=>$week),
                        array('option_item'=>'report_week'));
            }

            $week = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='report_week'");
            $window = $this->generateWeekWindow($week);
            $start = $window['Start'];
            $end = $window['End'];
            $prev =  $window['Prev'];
            $next =  $window['Next'];
			$view->report_week = $start;


            $view->prev = $view->request_path . "&week=" .$prev;
            $view->week = $start;
            $view->next = $view->request_path . "&week=" . $next;


            $sql = "SELECT r.team_id,t.name AS team_name, t.division, count(*) AS resv_count "
				   . " FROM " . TSLL_RESERVATIONS_TABLE . " r "
                   . " INNER JOIN " . TSLL_TEAMS_TABLE . " t ON r.team_id = t.id "
				   . " WHERE DATE(gamedate) >= '$start' AND DATE(gamedate) < '$end' "
				   . " GROUP BY r.team_id ORDER BY t.division,team_name";

            $view->reservations = $wpdb->get_results($sql,ARRAY_A);

			$view->nextWeek = $this->human_dates['ThisWeek'];
            $view->fieldCheckBoxesHtml = $this->generateHtmlFieldsForm();

            include_once($view_path . '/list.phtml');

        }

        /**
         * displayMenuManageRequests
         *
         * Take a looke at the requests we're receiving.
         *
         */
        function displayMenuManageRequests()
        {
            global $wpdb;
            $view = new stdClass();
            $view->cancel_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsll-requests';

            /** including renders the views **/
            print "<div class='wrap'><div id='icon-teams' class='icon32'></div><h2>Manage Requests</h2></div><br/>";

            $action = $this->getParam('action');

            if ($action) {
                $id = $this->getParam('id');
                $team_id = $this->getParam('team_id');
                $req1 = $this->getParam('req1');
                $req2 = $this->getParam('req2');
                $req3 = $this->getParam('req3');
                $req4 = $this->getParam('req4');
                $req5 = $this->getParam('req5');
                $req6 = $this->getParam('req6');
                $created = $this->getParam('created',date('Y-m-d G:i:s'));
                $modified = $this->getParam('modified',date('Y-m-d G:i:s'));
            }

            switch ($action) {


                case 'Edit':
                    $sql = "SELECT * FROM " . TSLL_REQUESTS_TABLE . " WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->request = $record[0];
                    $view->action = "Update";
                    break;

                case 'Update':
                    // WPINSERT sanitizes the data by escaping the values.
                    $data  = array( 'team_id'=>$team_id,
                                    'req1'=>$req1,
                                    'req2'=>$req2,
                                    'req3'=>$req3,
                                    'req4'=>$req4,
                                    'req5'=>$req5,
                                    'req6'=>$req6,
                                    'modified'=>$modified);
                    $where = array('id'=>$id);
                    $results = $wpdb->update( TSLL_TEAMS_TABLE,$data,$where );
                    print "<em>Updated Request: $id</em>";
                    $view->request = null;
                    $view->action = "Add";
                    break;

                case 'Delete':
                    $sql = "SELECT * FROM " . TSLL_REQUESTS_TABLE . " WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->request = $record[0];
                    $view->action = 'ConfirmDelete';
                    include_once('views/requests/delete.phtml');
                    return;
                    break;

                case 'ConfirmDelete':
                    // No love, you delete it, and that's it!
                    $sql = "SELECT * FROM " . TSLL_REQUESTS_TABLE . " WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->request = $record[0];
                    $team_name = $wpdb->get_var("SELECT name FROM " . TSLL_TEAMS_TABLE . " WHERE id=".$view->request['team_id']);

                    $sql = "DELETE FROM " . TSLL_REQUESTS_TABLE . " WHERE id=$id";
                    $wpdb->get_results($sql);

                    print "<em>Deleted Field Request ($id) For: $team_name</em> <br/>";
                    print "<a href='" . get_option('siteurl') . "/wp-admin/admin.php?page=tsll-requests" . "'>Done</a>";
                    return;
                    break;

                case 'Add':
                    /* User submitted a form to add a team */
                    $team_name = $wpdb->get_var("SELECT name FROM " . TSLL_TEAMS_TABLE . " WHERE id=".$view->request['team_id']);
                    // WPINSERT sanitizes the data by escaping the values.
                    $results = $wpdb->insert( TSLL_REQUESTS_TABLE, array( 'team_id'=>$team_id,
                                                            'req1'=>$req1,
                                                            'req2'=>$req2,
                                                            'req3'=>$req3,
                                                            'req4'=>$req4,
                                                            'req5'=>$req5,
                                                            'req6'=>$req6,
                                                            'created'=>$created,
                                                            'modified'=>$modified));

                    print "<em>Added Request For: $team_name</em>";
                    $view->request = null;
                    $view->action = "Add";
                    break;

                default:
                    $view->request = null;
                    $view->action = "Add";
            }


            /** Show the list **/

            $sql = "SELECT r.id,
                        t.name,
                        t.division,
                        r.req1,
                        r.req2,
                        r.req3,
                        r.req4,
                        r.req5,
                        r.req6,
                        r.created
                    FROM " . TSLL_REQUESTS_TABLE . " r
                    INNER JOIN " . TSLL_TEAMS_TABLE . " t ON (r.team_id=t.id)
                    INNER JOIN (
                    SELECT team_id,MAX(created) max_created FROM ". TSLL_REQUESTS_TABLE ." GROUP BY team_id) l
                    ON (r.team_id=l.team_id AND r.created=l.max_created)";

            $view->requests = $wpdb->get_results($sql,ARRAY_A);

            include_once('views/requests/list.phtml');


        }

        /**
         * The is the best and most robust module I have.  It handles everything you want for
         * editing records.
         *
         * Most of the html is included in view scripts.
         */
        function displayMenuManageTeams()
        {
            global $wpdb;
            $view = new stdClass();
            $view->cancel_path = get_option('siteurl') . '/wp-admin/admin.php?page=mg-teams';

            /** including renders the views **/
            print "<div class='wrap'><div id='icon-teams' class='icon32'></div><h2>Manage Teams</h2></div><br/>";

            $action = $this->getParam('action');
            if ($action) {
                $id = $this->getParam('id');
                $name = $this->getParam('name');
                $manager = $this->getParam('manager');
                $division = $this->getParam('division');
                $email = $this->getParam('email');
                $created = $this->getParam('created',date('Y-m-d G:i:s'));
                $modified = $this->getParam('modified',date('Y-m-d G:i:s'));
            }

            switch ($action) {


                case 'Edit':
                    $sql = "SELECT * FROM " . TSLL_TEAMS_TABLE . " WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->team = $record[0];
                    $view->action = "Update";
                    break;

                case 'Update':
                    // WPINSERT sanitizes the data by escaping the values.
                    $data  = array( 'name'=>$name,
                                    'manager'=>$manager,
                                    'division'=>$division,
                                    'email'=>$email,
                                    'modified'=>$modified);
                    $where = array('id'=>$id);
                    $results = $wpdb->update( TSLL_TEAMS_TABLE,$data,$where );
                    print "<em>Updated Team: $name</em>";
                    $view->team = null;
                    $view->action = "Add";
                    break;

                case 'Delete':
                    $sql = "SELECT * FROM " . TSLL_TEAMS_TABLE . " WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->team = $record[0];
                    $view->action = 'ConfirmDelete';
                    include_once('views/teams/delete.phtml');
                    return;
                    break;

                case 'ConfirmDelete':
                    // No love, you delete it, and that's it!
                    $sql = "SELECT * FROM " . TSLL_TEAMS_TABLE . " WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->team = $record[0];
                    $name = $view->team['name'];

                    $sql = "DELETE FROM " . TSLL_TEAMS_TABLE . " WHERE id=$id";
                    $wpdb->get_results($sql);

                    print "<em>Deleted Team: $name</em> <br/>";
                    print "<a href='" . get_option('siteurl') . "/wp-admin/admin.php?page=mg-teams" . "'>Done</a>";
                    return;
                    break;

                case 'Add':
                    /* User submitted a form to add a team */

                    // WPINSERT sanitizes the data by escaping the values.
                    $results = $wpdb->insert( TSLL_TEAMS_TABLE, array( 'name'=>$name,
                                                           'manager'=>$manager,
                                                           'division'=>$division,
                                                           'email'=>$email,
                                                           'created'=>$created,
                                                           'modified'=>$modified));

                    print "<em>Added Team: $name</em>";
                    $view->team = null;
                    $view->action = "Add";
                    break;

                default:
                    $view->team = null;
                    $view->action = "Add";
            }


            /** Show the list **/
            $sql = "SELECT * FROM " . TSLL_TEAMS_TABLE;
            $view->teams = $wpdb->get_results($sql,ARRAY_A);
            $view->editSelector = $this->getHtmlDivisionSelect();

            include_once('views/teams/add_edit.phtml');
            include_once('views/teams/list.phtml');

        }



        /**
         * Manage the number of fields.
         * Handles managing the field names.
         */
        function displayMenuManageFields()
        {
            global $wpdb;
            $table_name = TSLL_FIELDS_TABLE;
            $view_path = 'views/fields/';
            $view = new stdClass();
            $view->cancel_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsll-fields';

            /** including renders the views **/
            print "<div class='wrap'><div id='icon-fields' class='icon32'></div><h2>Manage Fields</h2></div><br/>";

            $action = $this->getParam('action');
            if ($action) {
                $id = $this->getParam('id');
                $name = $this->getParam('name');
                $description = $this->getParam('description');
                $priority = $this->getParam('priority');
                $created = $this->getParam('created',date('Y-m-d G:i:s'));
                $modified = $this->getParam('modified',date('Y-m-d G:i:s'));
            }

            switch ($action) {


                case 'Edit':
                    $sql = "SELECT * FROM {$table_name} WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->field = $record[0];
                    $view->action = "Update";
                    break;

                case 'Update':
                    // WPINSERT sanitizes the data by escaping the values.
                    $data  = array( 'name'=>$name,
                                    'description'=>$description,
                                    'priority'=>$priority,
                                    'modified'=>$modified);

                    $where = array('id'=>$id);
                    $results = $wpdb->update( $table_name,$data,$where );
                    print "<em>Updated Field: $name</em>";
                    $view->field = null;
                    $view->action = "Add";
                    break;

                case 'Delete':
                    $sql = "SELECT * FROM {$table_name} WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->field = $record[0];
                    $view->action = 'ConfirmDelete';
                    include_once($view_path . 'delete.phtml');
                    return;
                    break;

                case 'ConfirmDelete':
                    // No love, you delete it, and that's it!
                    $sql = "SELECT * FROM {$table_name} WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->field = $record[0];
                    $name = $view->field['name'];

                    $sql = "DELETE FROM {$table_name} WHERE id=$id";
                    $wpdb->get_results($sql);

                    print "<em>Deleted Field: $name</em> <br/>";
                    print "<a href='" . get_option('siteurl') . "/wp-admin/admin.php?page=mg-fields" . "'>Done</a>";
                    return;
                    break;

                case 'Add':
                    /* User submitted a form to add a team */

                    // WPINSERT sanitizes the data by escaping the values.
                    $results = $wpdb->insert( $table_name, array( 'name'=>$name,
                                                           'description'=>$description,
                                                           'priority'=>$priority,
                                                           'created'=>$created,
                                                           'modified'=>$modified));

                    print "<em>Added Field: $name</em>";
                    $view->field = null;
                    $view->action = "Add";
                    break;

                default:
                    $view->field = null;
                    $view->action = "Add";
            }


            /** Show the list **/
            $sql = "SELECT * FROM {$table_name}";
            $view->fields = $wpdb->get_results($sql,ARRAY_A);

            include_once($view_path . 'add_edit.phtml');
            include_once($view_path . 'list.phtml');


        }


        function displayMenuManageGameTimes()
        {
            global $wpdb;

            /**
             * Setup the options - this could become a generic function
             */

            $table_name = TSLL_GAMETIMES_TABLE;
            $view_path = 'views/gametimes/';
            $label = "Game Time";
            $labels = "Game Times";
            $view = new stdClass();
            $view->gametime = null;
            $method = 'gametime';

            $view->cancel_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsll-gametimes';

            /** including renders the views **/
            print "<div class='wrap'><div id='icon-fields' class='icon32'></div><h2>Manage {$labels}</h2></div><br/>";

            $action = $this->getParam('action');
            if ($action) {
                $id = $this->getParam('id');
                $day = $this->getParam('day');
                $start_time = $this->getParam('start_time');
                $notes = $this->getParam('notes');
                $created = $this->getParam('created',date('Y-m-d G:i:s'));
                $modified = $this->getParam('modified',date('Y-m-d G:i:s'));
            }

            switch ($action) {


                case 'Edit':
                    $sql = "SELECT * FROM {$table_name} WHERE id=$id";
                    print "edit: id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->gametime = $record[0];
                    print " view-gt : {$view->gametime[id]}";
                    $day = $record[0]['day'];
                    $start_time=$record[0]['start_time'];
                    $view->action = "Update";
                    break;

                case 'Update':
                    // WPINSERT sanitizes the data by escaping the values.
                    $data  = array( 'day'=>$day,
                                    'start_time'=>$start_time,
                                    'notes'=>$notes,
                                    'modified'=>$modified);

                    $where = array('id'=>$id);
                    $results = $wpdb->update( $table_name,$data,$where );
                    print "<em>Updated [$id] $label: $day @ $start_time</em>";
                    $view->gametime = null;
                    $view->action = "Add";
                    break;

                case 'Delete':
                    $sql = "SELECT * FROM {$table_name} WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->gametime = $record[0];
                    $view->action = 'ConfirmDelete';
                    include_once($view_path . 'delete.phtml');
                    return;
                    break;

                case 'ConfirmDelete':
                    // user can only get this after seeing the delete confirmation form.
                    // No love, you delete it, and that's it!
                    $sql = "SELECT * FROM {$table_name} WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->gametime = $record[0];
                    $day = $view->gametime['day'];
                    $start_time = $view->gametime['start_time'];

                    $sql = "DELETE FROM {$table_name} WHERE id=$id";
                    $wpdb->get_results($sql);

                    print "<em>Deleted {$label}: $day @ $start_time</em> <br/>";
                    print "<a href='" . get_option('siteurl') . "/wp-admin/admin.php?page=tsll-gametimes'>Done</a>";
                    return;
                    break;

                case 'Add':
                    /* User submitted a form to add a team */

                    // WPINSERT sanitizes the data by escaping the values.
                    $results = $wpdb->insert( $table_name, array( 'day'=>$day,
                                                           'start_time'=>$start_time,
                                                           'notes'=>$notes,
                                                           'created'=>$created,
                                                           'modified'=>$modified));

                    print "<em>Added {$label}: $name</em>";
                    $view->gametime = null;
                    $view->action = "Add";
                    break;

                default:
                    $view->gametime = null;
                    $view->action = "Add";
            }


            /** Show the list **/
            $sql = "SELECT * FROM {$table_name}";
            $view->gametimes = $wpdb->get_results($sql,ARRAY_A);
            usort($view->gametimes,array(&$this,'sortByDay'));

            $view->selectDayHtml = $this->getHtmlDaySelect($day);
            $view->selectStartTimeHtml = $this->getHtmlTimeSelect($start_time);

            include_once($view_path . 'add_edit.phtml');
            include_once($view_path . 'list.phtml');

        }

        function displayMenuOptions()
        {
            global $wpdb;

            /**
             * Setup the options - this could become a generic function
             */

            $table_name = TSLL_GAMETIMES_TABLE;
            $view_path = 'views/options/';
            $label = "Game Time";
            $labels = "Game Times";
            $view = new stdClass();
            $view->options = null;

            $action = $this->getParam('action');

            if (empty($action)) {
                $open_requests_date = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='open_requests_date'");
                $open_requests_time = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='open_requests_time'");
                $close_requests_date = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='close_requests_date'");
                $close_requests_time = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='close_requests_time'");
                $display_week = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='display_week'");
                $report_week = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='report_week'");

            } else {
                $open_requests_date = $this->getParam('open_requests_date');
                $open_requests_time = $this->getParam('open_requests_time');
                $close_requests_date = $this->getParam('close_requests_date');
                $close_requests_time = $this->getParam('close_requests_time');
                $display_week = $this->getParam('display_week');
                $report_week = $this->getParam('report_week');
            }

            switch ($action) {
                case 'update':
                    $wpdb->update( TSLL_OPTIONS_TABLE,
                                  array('option_value'=>$open_requests_date),
                                  array('option_item'=>'open_requests_date'));
                    $wpdb->update( TSLL_OPTIONS_TABLE,
                                  array('option_value'=>$open_requests_time),
                                  array('option_item'=>'open_requests_time'));
                    $wpdb->update( TSLL_OPTIONS_TABLE,
                                  array('option_value'=>$close_requests_date),
                                  array('option_item'=>'close_requests_date'));
                    $wpdb->update( TSLL_OPTIONS_TABLE,
                                  array('option_value'=>$close_requests_time),
                                  array('option_item'=>'close_requests_time'));
					$wpdb->update( TSLL_OPTIONS_TABLE,
                                  array('option_value'=>$display_week),
                                  array('option_item'=>'display_week'));
					$wpdb->update( TSLL_OPTIONS_TABLE,
                                  array('option_value'=>$report_week),
                                  array('option_item'=>'report_week'));
                    break;

                default:
                    break;
            }
            $view->options['open_requests_date'] = $open_requests_date;
            $view->options['open_requests_time'] = $open_requests_time;
            $view->options['close_requests_date'] = $close_requests_date;
            $view->options['close_requests_time'] = $close_requests_time;
            $view->options['display_week'] = $display_week;
            $view->options['report_week'] = $report_week;
            $view->selectOpenTimeHtml = $this->getHtmlTimeSelect($open_requests_time,'open_requests_time','open_requests_time_select');
            $view->selectCloseTimeHtml = $this->getHtmlTimeSelect($close_requests_time,'close_requests_time','close_requests_time_select');
            include_once( $view_path . 'options.phtml');
        }

        /**
         * displayMenuManageReservations
         * This function displays the forms and interactive tables that manage
         * field reservations for a specific week.
         * Probably the most important piece in the backend!
         *
         * It requires several external files to list the table.
         */
        function displayMenuManageReservations()
        {
            global $wpdb;
            $view = new stdClass();
			$table_name = TSLL_RESERVATIONS_TABLE;
            $view_path = 'views/reservations/';
            $label = "Reservation";
            $labels = "Reservations";
            $view = new stdClass();
            $view->reservation = null;
            $method = 'reservation';
            $view->cancel_path = get_option('siteurl') . '/wp-admin/admin.php?page=tsll-reservations';
            $view->request_path = $view->cancel_path;

            print "<div class='wrap'><div id='icon-fields' class='icon32'></div><h2>Manage {$labels}</h2></div><br/>";

			$action = $this->getParam('action');

            if (!empty($action) && $action != 'Generate') {
                $id = $this->getParam('id');
                $gamedate = $this->getParam('gamedate');
                $start_time = $this->getParam('start_time');
                $team_id = $this->getParam('team_id');
                $where = $this->getParam('where');
                $field_id = $this->getParam('field_id');
                $created = $this->getParam('created',date('Y-m-d G:i:s'));
                $modified = $this->getParam('modified',date('Y-m-d G:i:s'));


            } else {
                $id = null;
                $gamedate = null;
                $start_time = null;
                $team_id = null;
            }


			switch ($action) {
				case 'Generate':
					$start_date = $this->getParam('start_date');
					$fields = $this->getParam('include_fields',null,false);  // don't escape the value or the array is LOST.
					$view->start_date = $start_date;
					$this->generateFieldTimes($start_date,$fields);
					break;

                case 'Wipe':
                    // wipe out this weeks records to start OVER
                    if (current_user_can('level_10')) {
                        $report_week = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='report_week'");
                        $utime = strtotime($report_week);
                        $window = array('NextWeek'=> date('Y-m-d',$utime),
                                        'EndOfNextWeek'=> date('Y-m-d',strtotime("+7 days",$utime)));
                        $sql = "DELETE FROM " . TSLL_RESERVATIONS_TABLE
                               . " WHERE DATE(gamedate) >= '{$window['NextWeek']}' AND DATE(gamedate) < '{$window['EndOfNextWeek']}' ";
                        $wpdb->query($sql);
                    }


                case 'Edit':
                    $sql = "SELECT * FROM {$table_name} WHERE id=$id";
                    print "edit: id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->reservation = $record[0];
                    $gamedate = date('m/d/Y',strtotime($view->reservation['gamedate']));
                    $start_time = date('G:i:00',strtotime($view->reservation['gamedate']));
                    $field_id = $view->reservation['field_id'];
                    $team_id = $view->reservation['team_id'];
                    $view->action = "Update";
                    break;


                case 'BatchEdit':
                    $records = $this->getParam('record_id',array(),false);
                    $where = " WHERE id in ( " . join(",",$records) . " ) ";
                    $view->recordCount = count($records);
                    $view->where = $where;
                    $team_name = $wpdb->get_var("SELECT CONCAT(name,'(',division,')') name_div FROM " . TSLL_TEAMS_TABLE . " WHERE id=$team_id");
                    $view->team_name = $team_name;
                    $view->team_id = $team_id;
                    $view->action = 'ConfirmUpdate';
                    include_once($view_path . 'confirm.phtml');
                    return;
                    break;

                case 'ConfirmUpdate':
                    $wpdb->query('UPDATE ' . TSLL_RESERVATIONS_TABLE . " SET team_id=$team_id " . $where);
                    break;


                case 'Update':
                    // WPINSERT sanitizes the data by escaping the values.
                    $gamedate = date('Y-m-d',strtotime($gamedate));
                    $data  = array( 'gamedate'=>$gamedate . " " . $start_time,
                                    'field_id'=>$field_id,
                                    'team_id'=>$team_id,
                                    'modified'=>$modified);

                    $where = array('id'=>$id);
                    $results = $wpdb->update( $table_name,$data,$where );
                    print "<em>Updated [$id] $label: $day @ $start_time</em>";
                    $view->reservation = null;
                    $view->action = "Add";
                    break;

                case 'Accept':
                    // WPINSERT sanitizes the data by escaping the values.
                    $sql = "SELECT * FROM {$table_name} WHERE id=$id";
                    $record = $wpdb->get_results($sql,ARRAY_A);
                    $view->reservation = $record[0];
                    $gamedate = date('Y-m-d',strtotime($view->reservation['gamedate']));
                    $data  = array( 'gamedate'=>$gamedate . " " . $start_time,
                                    'field_id'=>$field_id,
                                    'team_id'=>$team_id,
                                    'modified'=>$modified);

                    $where = array('id'=>$id);
                    $results = $wpdb->update( $table_name,$data,$where );
                    print "<em>Accepted Change To [$id] $label: $day @ $start_time</em>";
                    $view->reservation = null;
                    $view->action = "Add";
                    break;

                case 'QDel':
                    // quick delete.
                    $wpdb->query("DELETE FROM {$table_name} WHERE id=$id");
                    break;

                case 'batchDelete':
                    //
                    $records = $this->getParam('record_id',array(),false);
                    $where = " WHERE id in ( " . join(",",$records) . " ) ";
                    $view->recordCount = count($records);
                    $view->where = $where;
                    $view->action = 'ConfirmDelete';
                    include_once($view_path . 'delete.phtml');
                    return;
                    break;

                case 'ConfirmDelete':
                    // This is a batch delete. $where needs to have the where clause.
                    $where = $this->getParam('where');
                    $wpdb->query("DELETE FROM {$table_name} $where");
                    break;

                case 'Add':
                    // WPINSERT sanitizes the data by escaping the values.
                    $gamedate = date('Y-m-d',strtotime($gamedate));
                    $results = $wpdb->insert( $table_name, array( 'gamedate'=>$gamedate . " " . $start_time,
                                                           'field_id'=>$field_id,
                                                           'team_id'=>$team_id,
                                                           'created'=>$created,
                                                           'modified'=>$modified));

                    print "<em>Added {$label}: $name</em>";
                    $view->gametime = null;
                    $view->action = "Add";
                    break;
                default:
                    $view->gametime = null;
                    $view->action = "Add";
			}

            /**
             * Components for rendering the reservation add/edit forms
             */
            $view->selectTimeHtml = $this->getHtmlTimeSelect($start_time);
            $view->selectTeamHtml = $this->getHtmlTeamSelect($team_id);
            $view->batchSelectTeamHtml = $this->getHtmlTeamSelect($team_id,'batch_team_select');
            $view->selectFieldHtml = $this->getHtmlFieldSelect($field_id);
            $view->gamedate = $gamedate;

			/*
            $report_week = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='report_week'");
			$utime = strtotime($report_week);
			$window = array('NextWeek'=> date('Y-m-d',$utime),
							'EndOfNextWeek'=> date('Y-m-d',strtotime("+7 days",$utime)));
            */

            $week = $this->getParam('week',null);

            if ($week) {
                /* if week is defined save it.  That way we always come back to the right spot*/
                $wpdb->update( TSLL_OPTIONS_TABLE,
                        array('option_value'=>$week),
                        array('option_item'=>'report_week'));
            }

            $week = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='report_week'");
            $window = $this->generateWeekWindow($week);
            $start = $window['Start'];
            $end = $window['End'];
            $prev =  $window['Prev'];
            $next =  $window['Next'];
            $view->report_week = $start;
            $view->summary_html= $this->generateNewReservationTable(true,$start);
            $view->prev = $view->request_path . "&week=" .$prev;
            $view->week = $start;
            $view->next = $view->request_path . "&week=" . $next;


            $sql = "SELECT r.*,f.name AS field_name, t.name as team_name, t.division FROM " . TSLL_RESERVATIONS_TABLE . " r "
                   . " INNER JOIN " . TSLL_TEAMS_TABLE . " t ON r.team_id = t.id "
                   . " INNER JOIN " .  TSLL_FIELDS_TABLE . " f ON r.field_id = f.id "
				   . " WHERE DATE(gamedate) >= '$start' AND DATE(gamedate) < '$end' "
				   . " ORDER BY r.gamedate, field_name";
            $view->reservations = $wpdb->get_results($sql,ARRAY_A);

			//$window = $this->generateNextWeekWindow(1,DATE_HUMAN);
			$view->nextWeek = $this->human_dates['ThisWeek'];
            $view->fieldCheckBoxesHtml = $this->generateHtmlFieldsForm();

            /**
             * Generate a list of all resevations by week - summary view for jumping to a certain week
             * Note: This is the logic that gets the week summaries, would be nice to put this in the summary
             * page.
             */
            $year = date('Y'); // restrict to THIS year.
            $sql = "select WEEK(gamedate) as
                    ww,sum(if(team_id=1,1,0)) open_fields,count(*) as total_fields
                    FROM `wp_tsll_reservations`
                    WHERE YEAR(gamedate)= $year
                    GROUP BY ww";
            $view->reservation_summary = $wpdb->get_results($sql,ARRAY_A);
            include_once($view_path . '/list.phtml');
        }

		function generateFieldTimes($start_date,$include_fields=null)
		{
            global $wpdb;
            $table_name = TSLL_RESERVATIONS_TABLE;

			if (empty($include_fields)) {
				// default is ALL fields
				$where = "";
			} else {
				$list =  implode(",",$include_fields);
				$where = " WHERE id in ($list)";
			}

			$sql = "SELECT * FROM " . TSLL_FIELDS_TABLE . $where;
			$fields = $wpdb->get_results($sql,ARRAY_A);

            $sql = "SELECT * FROM " . TSLL_GAMETIMES_TABLE;
            $gametimes = $wpdb->get_results($sql,ARRAY_A);
            usort($gametimes,array(&$this,sortByDay));

            $udate = strtotime($start_date);

            $day = strtoupper(date("l",$udate));
            while ($day != 'MONDAY') {
                $udate = strtotime('-1 day',$udate); // backup to Monday.
                $day = strtoupper(date("l",$udate));
            }

            $check_sql = "SELECT level FROM wp_tsll_games ";
            foreach ($gametimes as $gametime) {

                while ($gametime['day'] != $day) {
                    $udate = strtotime('+1 day',$udate); // backup to Monday.
                    $day = strtoupper(date("l",$udate));
                }

                foreach ($fields as $field) {

                    $gdate = date('Y-m-d',$udate);
                    $gtime = $gametime['start_time'];
                    $gamedate = "$gdate $gtime";

                    $field_id = $field['id'];
                    $field_name = $field['name'];
                    $created = date('Y-m-d G:i:s');
                    $modified = date('Y-m-d G:i:s');

                    // Is there a game now?
                    $where = " WHERE game_date='$gdate' AND game_time='$gtime' AND field='$field_name'";

                    $game_level = $wpdb->get_var($check_sql . $where);
                    if (empty($game_level)) {
                        $team_id = OPEN_TEAM;

                    } else {
                        switch ($game_level) {
                            case 'majors': $team_id=2;  break;
                            case 'minors' : $team_id = 3; break;
                            case 'aaa' : $team_id = 4; break;
                            case 'aa' : $team_id = 5; break;
                            default:
                                $team_id = OPEN_TEAM;
                        }
                    }

                    $results = $wpdb->insert( TSLL_RESERVATIONS_TABLE, array( 'gamedate'=>$gamedate,
                                                           'field_id'=>$field_id,
                                                           'team_id'=>$team_id,
                                                           'created'=>$created,
                                                           'modified'=>$modified));


                }
            }

		}

		/**
		 * generateFieldsForm
		 * creates html for a box of check lists that indicates which fields to
		 * include when generating a list of fields for reservations.
		 */
		function generateHtmlFieldsForm()
		{
			global $wpdb;

			$sql = "SELECT * FROM " . TSLL_FIELDS_TABLE . " ORDER BY priority,name ";
			$fields = $wpdb->get_results($sql,ARRAY_A);
			$html = "<fieldset id='fields_div'>
					<legend>Fields To Include:</legend>";
			foreach ($fields as $field) {
				$html .= "<input type='checkbox' checked='checked' value='{$field['id']}' name='include_fields[]' /> {$field['name']}&nbsp;";
			}
			$html .= "</fieldset>";
			return $html;
		}

        /*
         * Gets a passed parameter or returns the default
         * ZendLike
         */
        function getParam($var,$default=null,$escaped=true)
        {
           $value = isset($_REQUEST[$var]) ? $_REQUEST[$var] : $default;
           if ($escaped) $value = mysql_escape_string($value);
           return $value;
        }


        function getHtmlDivisionSelect()
        {
            $enum_fields = $this->getEnumAsArray(TSLL_TEAMS_TABLE,'division');
            $select = "<select name='division' id='division_select'>";
            foreach ($enum_fields as $enum) {
                $select .= "<option value='$enum'>".ucwords( strtolower($enum) )."</option>";
            }
            $select .="</select>";
            return $select;
        }

        function getHtmlTeamSelect($team_id,$ctrl_id='team_select')
        {
            global $wpdb;
            $sql = "SELECT id,name,division,CONCAT(name,' [',division,']') full_name FROM " . TSLL_TEAMS_TABLE . " ORDER BY division DESC, name ASC ";
            $teams = $wpdb->get_results($sql,ARRAY_A);

            if (empty($teams)) return "<div class='error'>There are no teams defined!</div>";

            $select = "<select name='team_id' id='$ctrl_id'>";
            foreach ($teams as $team) {
                $selected = ($team['id']==$team_id) ? 'selected="selected"' : "";
                $select .= "<option value='{$team['id']}' {$selected}>".ucwords( strtolower($team['full_name']) )."</option>";
            }
            $select .="</select>";
            return $select;
        }

        function getHtmlFieldSelect($field_id)
        {
            global $wpdb;
            $sql = "SELECT id,name FROM " . TSLL_FIELDS_TABLE;
            $fields = $wpdb->get_results($sql,ARRAY_A);

            $select = "<select name='field_id' id='field_select'>";
            foreach ($fields as $field) {
                $selected = ($field['id']==$field_id) ? 'selected="selected"' : "";
                $select .= "<option value='{$field['id']}' {$selected}>{$field['name']}</option>";
            }
            $select .="</select>";
            return $select;
        }

        function getHtmlDaySelect($day)
        {
            $enum_fields = $this->getEnumAsArray(TSLL_GAMETIMES_TABLE,'day');
            $select = "<select name='day' id='day_select'>";
            foreach ($enum_fields as $enum) {
                if (!empty($day) && $day==$enum) $selected = 'selected="selected"'; else $selected = "";

                $select .= "<option value='{$enum}' {$selected}>".ucwords( strtolower($enum) )."</option>";
            }
            $select .="</select>";
            return $select;
        }

        function getHtmlTimeSelect($start_time,$name='start_time',$id='start_time_select')
        {
            $curr_time = mktime(8,0,0);  // start at 8:00 AM
            $select = "<select name='$name' id='$id'>";
            for ($t=0; $t<32;$t++) {
                $value = date('H:i:00',$curr_time);
                $option = date('h:i A',$curr_time);

                if (!empty($start_time) && $start_time==$value) $selected = 'selected="selected"'; else $selected = "";

                $select .= "<option value='{$value}' {$selected}>{$option}</option>";
                $curr_time += 30 * 60;  // add 30 minutes to the time.
            }
            $select .="</select>";
            return $select;
        }

        /**
         * Use a clever regular expression to get the enum values from a table column
         */
        function getEnumAsArray($table,$field)
        {
            global $wpdb;
            $sql = "SHOW COLUMNS FROM " . $table . " LIKE '" . $field . "'";
            $results = $wpdb->get_results($sql,ARRAY_A);
            $regex = "/'(.*?)'/";
            preg_match_all( $regex, $results[0]['Type'], $enum_array);
            $enum_fields = $enum_array[1];
            return $enum_fields;
        }

        /**
         * generateResults
         * display a table with a list of results.
         */
        function generateResults() {

            $mode = $this->getParam('mode',null);
            $requests_path = get_bloginfo('wpurl') . "/results";
            if (empty($mode)) {
                $raw_html = file_get_contents(  ABSPATH . "wp-content/plugins/tsllfields/html/results.html");
                $html = str_replace('{REQUEST_PATH}',$request_path,$raw_html);
            } else {
                switch ($mode) {
                    case 'scores':
                        $html = $this->generateGameScores();
                        break;
                    case 'view':
                        $html = $this->viewGame();
                        break;

                    case 'list':
                    case 'schedule':
                    default:
                        $html = $this->generateGameList();
                        break;
                }
            }

            return $html;
        }

        function generateGameScores() {
            global $wpdb;
            $requests_path = get_bloginfo('wpurl') . "/results";


            // get list of all scores PRIOR to today
            $cutoff = $this->getParam('game_date',date('Y-m-d'));

            $where = "game_date <= '$cutoff'";
            $orderby = " ORDER BY home ";

            $sql = "SELECT
                        id,
                        DATE_FORMAT(game_date,'%m/%d (%a)') gdate,
                        home,
                        away,
                        home_score,
                        away_score,
                        field FROM " . TSLL_GAMES_TABLE . " WHERE ". $where
                        . $orderby;
            $games = $wpdb->get_results($sql,ARRAY_A);
            $html = "<table id='tsll_scores'>";
            $html .= "<thead><tr>";
            $html .= "<th>Date</th><th>Game</th><th>Score</th>";
            $html .= "</tr></thead>";
            $html .= "<tbody>";
            foreach ($games as $game) {

                if ($game['home_score']>$game['away_score']) {
                    $game['home'] .= "(<b>W</b>)";
                    $game['away'] .="(L)";
                }
                else if ($game['home_score']==$game['away_score']){
                    if ($game['home_score'] != null) {
                        $game['home'] .="(T)";
                        $game['away'] .="(T)";
                    }
                } else {
                    $game['home'] .= "(L)";
                    $game['away'] .= "(<b>W</b>)";
                }

                $date_cell = $game['gdate'] ;
                $game_cell = $game['away'] . " vs " . $game['home'] ;
                $score_cell = $game['away_score'] . "-" . $game['home_score'];

                $game_cell = "<a href='".$request_path . "?mode=view&id=".$game['id']."'>".$game_cell."</a>";
                $html .="<tr><td>$date_cell</td><td>$game_cell</td><td>$score_cell<td></tr>";
            }
            $html .="</tbody>";
            $html .="</table>";
            return $html;
        }

        function generateGameList() {
            global $wpdb;


            $columns = array(
                array("Home",'home'),
                array("Away",'away'),
                array("Date",'gdate'),
                array("Time","gtime"),
                array("Field","field"),
              );

            $level = $this->getParam('level','majors');
            $where = "level='$level'";
            $orderby = " ORDER BY game_date";

            $sql = "SELECT
                        home,
                        away,
                        DATE_FORMAT(game_date,'%a (%m/%d)') as gdate,
                        TIME_FORMAT(game_time,'%h:%i %p') as gtime,
                        field FROM " . TSLL_GAMES_TABLE . " WHERE ". $where
                        . $orderby;
            $games = $wpdb->get_results($sql,ARRAY_A);
            $html = "<table id='tsll_games'>";
            $html .= "<thead><tr>";
            foreach($columns as $col) {
                $html .= "<th>". $col[0] ."</th>";
            }
            $html .= "</tr></thead>";
            $html .= "<tbody>";
            foreach ($games as $game) {
                $html .="<tr>";
                foreach ($columns as $col) {
                    $html .= "<td>".$game[$col[1]]."</td>";
                }
                $html .="</tr>";
            }
            $html .="</tbody>";
            $html .="</table>";
            return $html;
        }

        function viewGame() {
            global $wpdb;

            $id = $this->getParam('id',null);
            if (empty($id)) {
                $html = "<span class='ERROR'>Invalid Game ID</span>";
                return $html;
            }

            $sql = "SELECT * FROM " . TSLL_GAMES_TABLE . " WHERE id=$id";
            $games = $wpdb->get_results($sql,ARRAY_A);
            $game = $games[0];
            $html = "<h3>Game #$id: {$game['away']} vs {$game['home']}</h3>";
            $html .= "<p><strong>Date/Time</strong>: {$game['game_date']} @ {$game['game_time']}</p>";
            $html .= "<dl class='score'><dt>Score</dt><dd>";
            $html .= $game['away'] . " : " . $game['away_score'] . "<br/>";
            $html .= $game['home'] . " : " . $game['home_score'] . "<br/>";
            $html .= "</dd></dl><hr/><h3>Notes</h3><div class='notes'>{$game['notes']}</div><hr/>";
            return $html;
        }
        /**
         * headerInit
         * make sure jQuery is queued up.
         */

        function headerInit() {
			wp_enqueue_script('jquery');  // make sure this gets loaded.
		}


        /**
         * Sort an array using day name as the index
         *
         * Note, the smarter way to do this would have been to use a date time
         * object to include BOTH the day and the time.  Then we could have split
         * it up for easier sorting.
         */
        function sortByDay($a,$b) {
            $DAYS = array('MONDAY'=>0,
                              'TUESDAY'=>1,
                              'WEDNESDAY'=>2,
                              'THURSDAY'=>3,
                              'FRIDAY'=>4,
                              'SATURDAY'=>5,
                              'SUNDAY'=>6);
            $a_val = $DAYS[$a['day']];
            $b_val = $DAYS[$b['day']];
            if ($a_val == $b_val) {
                $a_t_val = $a['start_time'];
                $b_t_val = $b['start_time'];
                $atime =strtotime($a_t_val);
                $btime =strtotime($b_t_val);
                if ($atime==$btime) return 0;
                else
                    return ($atime < $btime) ? -1 : 1;
            } else
                return ($a_val < $b_val) ? -1 : 1;

        }

        /**
         * We can set when we protect fields for next week.
         * This function converts the generic dates and times in the database
         * to dates this week.  If we are in the window then next week requests
         * are protected and teams can get at most one field.
         * After that we go to first come first serve.
         *
         * Note: The open_request_date: the important thing is the day of the week.
         * close_request_date: is the date when fields stop being protected.
         */
		function setReservationRules()
		{
			global $wpdb;

			$open_requests_date = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='open_requests_date'");
			$open_requests_time = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='open_requests_time'");
			$close_requests_date = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='close_requests_date'");
			$close_requests_time = $wpdb->get_var("SELECT option_value FROM " . TSLL_OPTIONS_TABLE . " WHERE option_item='close_requests_time'");

			$open_timestamp = strtotime($open_requests_date . " " . $open_requests_time);
			$close_timestamp = strtotime($close_requests_date . " " . $close_requests_time);

            $open_day_offset = date("w",$open_timestamp);
            $close_day_offset = date("w",$close_timestamp);


            $open_day = strtotime("-1 day",$this->this_week_start); // backup a day
            $open_day = strtotime("+$open_day_offset days",$open_day);

            $open_day_timestamp = strtotime(date('Y-m-d',$open_day) . " " . $open_requests_time);
            $od = date('D, M-d, Y H:i A',$open_day_timestamp);

            $close_day = strtotime("-1 day",$this->this_week_start); // backup a day
            $close_day = strtotime("+$close_day_offset days",$close_day);

            $close_day_timestamp = strtotime(date('Y-m-d',$close_day) . " " . $close_requests_time);
            $cd = date('D M-d, Y H:i A',$close_day_timestamp);
            $this->protect_dates = array('Start'=>$open_day_timestamp,
                                    'End'=> $close_day_timestamp,
                                    'Start_Label'=>$od,
                                    'End_Label'=>$cd);

            /**
             * Here's how this logic works.  The day of the week calculated by the open_request_date
             * is used to pick a day "this week" when fields will be open for reservation.
             * the open_request_time is added to that date to calculate the exact moment fields
             * are available for request.
             * the day of the week calculated from the close_request_date determines when
             * "protection" of fields (one per team) ends.
             * the close_request_time is added to the close_request_date to determine the exact
             * time the protected window is over.
             * If NOW is BEFORE the open_day_timestamp, then the fields are locked.
             * if Now is in the middle of the window, the fields are protected (1 per team)
             * In all other cases... the fields are First Come First Serve.
             * So it doesn't matter the exact date of the open_request_date or close_request_date
             * it is the day of the week those dates represent.
             * There is no error checking of those days DON'T overlap (say open Friday but close Wednesday)
             */
            if ($this->now < $open_day_timestamp)
                $this->resv_rule = RULE_CLOSED;
            else if ($this->now > $open_day_timestamp and $this->now < $close_day_timestamp)
                $this->resv_rule = RULE_PROTECTED;
            else
                $this->resv_rule = RULE_FCFS;
		}

        /**
         * generateNextWeekWindow
         * generate week boundaries for a query.
         * If you give a date we will return the next week Monday to Sunday boundaries.
         * @param $date - must be in one of two formats YYYY-MM-DD or MM/DD/YYYY.
         */
        function generateWeekWindow($date=null,$format=DATE_MYSQL)
        {
            $window = array('NextWeek'=>'','EndOfNextWeek'=>'','nw_utime'=>0,'eonw_utime'=>0);

            $utime=null;

            if ($date) {
                // we've been given a specific date to work with.
                $match = array();
                $year = null; $month = null; $day = null;
                if (preg_match('/(\d{4})-(\d{2})-(\d{2})/',$date,$match)) {
                    $year = $match[1];
                    $month = $match[2];
                    $day = $match[3];
                } else if (preg_match('/(\d{2})\/(\d{2})\/(\d{4})/',$date,$match)) {
                    $year = $match[3];
                    $month = $match[1];
                    $day = $match[2];
                }

                if ($year) {
                    /* Normalize this to a Monday.  We will display the week specified,
                    *but it MUST start on a monday.
                    * 0  1  2  3  4  5  6  0  1
                    * S  M  T  W  T  F  S  S  M
                    * 21 22 23 24 25 26 27 28 01
                    *
                    * delta = 1 - w.  So feb-24 -> 1-3= -2 + 24 = Feb-22 Monday!
                    */
                    $utime = strtotime("$month/$day/$year");
                    $day_delta = 1-date("w",$utime);
                    $utime = strtotime("+$day_delta days",$utime);
                }
            }

            if ($utime===null)
                $utime = $this->this_week_start;    // if no date or we couldn't parse the date default to system object.

            $window['start_utime'] = $utime;
            $window['Start'] = date($format,$utime); // this gets us Monday
            $window['end_utime'] = strtotime("+7 days",$utime);
            $window['End'] = date($format,$window['end_utime']);
            $window['Label'] = date('m/d/Y',$utime);
            $window['Prev'] = date($format,strtotime("-7 days",$utime));
            $window['Next'] = date($format,strtotime("+7 days",$utime));
            return $window;
        }

        /**
         * Darn near the heart of the whole thing.
         * This is major update.  Allow the user to click directly
         * on the cell they want to reserve.
         * Use jQuery to submit the request.
         *
         * Change the request mechanism to handle only 1 at a time.
         */
        function generateNewReservationTable($view_only=false,$date=null)
        {
            global $wpdb;

            /**
             * Okay, handle paging through weeks of data.  This is a bit odd, but the fastest
             * way to do this is to put it here.  Let this app look for the week parameter
             * passed by the caller and display the appropriate buttons.
             */
            if ($date) {
                $window = $this->generateWeekWindow($date,DATE_MYSQL);
                $start = $window['Start'];
                $end = $window['End'];
            } else {
                $start = $this->sql_dates['ThisWeekStart'];
                $end = $this->sql_dates['NextWeekEnd'];
            }


            $where = " WHERE r.gamedate >= '$start' AND r.gamedate < '$end'";

            if ($wpdb->get_var("SELECT COUNT(*) FROM " . TSLL_RESERVATIONS_TABLE . " r " . $where) == 0)
                return "<div class='ERROR'>No Field Reservations Have Been Defined For Next Week.</div>";


            // Let's get our fields.

            $sql = "SELECT * FROM " . TSLL_FIELDS_TABLE . " ORDER BY priority,name ";
            $fields = $wpdb->get_results($sql,ARRAY_A);

            /**
             * Display Check.  Are there fields with NO slots available?
             *
             * Data format:
             * fields = array()
             * each record is:
             * array (
                id=>int,
                name=>string,
                description=>string,
                priority=>int,
                created=>datetime,
                modified=>datetime
             )
             *
             * Remove fields that have no reservations during the time period
             * specified by $where
             */


            foreach ($fields as $f=>$field) {
                $sql = "SELECT COUNT(*) FROM " . TSLL_RESERVATIONS_TABLE . " r "
                . $where
                . " AND r.field_id=".$field['id'];
                // $count = $wpdb->get_var($wpdb->prepare($sql));  Don't prepare the query, there are no inputs.
                $count = $wpdb->get_var($sql);
                if (empty($count)) {
                   unset($fields[$f]);
                }
            }

            /**
             * insane query to get rowspan
             * How this works -we need to do 2 queries, first we need to get all the dates
             * AND their times in one list (the subquery)
             * then we want to total the number of times per day - the outer query.
             * THAT is our row span.
             * So the inner query splits the gamedate object into two separate pieces date and time
             * the outer query tells us how many times per day we have.  Crazy.
             *
             * Data format:
             * $rowspans = array()
             *
             * Each record = array(
                d=>w-d,                 // a special string where the day of the week and day of the month
                                            form a unique key. Don't know why I picked this, but it's really
                                            unlikely those two numbers will occur within 2 weeks of each other.
                                            In other words, the numerical week day and month day form a realitively
                                            unique key.  Unique enough.
                rowspan=>int            // the number of different times that occur on that day.
               )
             */
            /* TODO: this will need to be filtered by DATE at some point */
            $sql = "SELECT d,COUNT(*) AS rowspan FROM "
                    . " (SELECT DATE_FORMAT(gamedate,'%w-%d') d, DATE_FORMAT(gamedate,'%h:%i') AS t,COUNT(*) FROM " . TSLL_RESERVATIONS_TABLE . " r "
                    . " $where GROUP BY d,t ) AS tmp GROUP BY d";
            $rowspans = $wpdb->get_results($sql,ARRAY_A);
            $rowspan = array();

            // convert results to a simple lookup.
            foreach ($rowspans as $r) {
                $rowspan[$r['d']] = $r['rowspan'];
            }

            /**
             * These are the available "time slots" where a team can reserve a field.
             *
             * Data Format:
             * $slots = array()
             *
             * A single record = array(
                id=>int,
                gamedate=>datetime,
                field_id=>int,
                team_id=>int,
                created=>datetime,
                modified=>datetime,
                field_name=>string,
                team_name=>string,
                division=>string
             )
             **/
            $sql = "SELECT r.*,f.name AS field_name, t.name as team_name, t.division FROM " . TSLL_RESERVATIONS_TABLE ." r "
                   . " INNER JOIN " . TSLL_TEAMS_TABLE . " t ON r.team_id = t.id "
                   . " INNER JOIN ". TSLL_FIELDS_TABLE . " f ON r.field_id = f.id "
                   . $where
                   . " ORDER BY r.gamedate";
            $slots = $wpdb->get_results($sql,ARRAY_A);

            // Matrix will be ROW priority, we define the ROWS first
            // Rows are by game date.
            // Columns are by fields.
            $matrix = array();

            // flesh out the array - on row per date.  Brute Force but simple.
            // An optimization would be to find out how many times we needlessly set a cell to null.
            foreach ($slots as $slot) {
                $matrix[$slot['gamedate']] = null;
            }

            // Now we create the columns.
            // Each $key in the matrix is a row by date and time.
            // Now we use the previously build fields and expand the matrix into columns.
            // now make sure every gamedate represents ALL fields.
            foreach ($matrix as $key=>$value) {
                foreach ($fields as $field)
                    $matrix[$key][$field['name']] = "(void)";
            }

            // With Matrix setup to be indexed by row/column (datetime/field)
            // we can expand our slots into this tabl - thus creating a simple pivot.
            // Now display the assignements
            foreach ($slots as $slot) {
                $matrix[$slot['gamedate']][$slot['field_name']] = array('team'=>$slot['team_name'],'division'=>$slot['division'],'id'=>$slot['id'],'held_by'=>$slot['team_id']);
            }

            $html = "<div class='resv_panel'>";
            $html .= "<table id='resv_table' class='RESV_TABLE' border='1px solid black' cellspacing='0' cellpadding='2px'>";
            $html .= "<TR style='text-align: center;'>";
            $html .= "<th class='DATE_HEADER'>Date</th><th class='TIME_HEADER'>Time</th>";

            // this will change - will need to include a special javascript file.
            // if ($view_only == false) $html .= "<th class='REQ_HEADER'>Req</th>";

            // generate the field count
            foreach ($fields as $field) {
                $html .= "<TH class='FIELD_HEADER'>{$field['name']}</TH>";
            }
            $html .= "</TR>";

            $last_date = null;
            $row_class = '';

            /**
             * We walk through the table a ROW at at time.  We get the row ($day), but the $locations is
             * another array full of data.
             */
            foreach ($matrix as $day=>$locations) {
                $uDay = strtotime($day);
                $date = date('D (m/d)',$uDay);
                $time = date('h:i A',$uDay);

                // Set how the rows will be displayed.
                if ($view_only == false and ($uDay >= $this->next_week_start and $uDay <= $this->next_week_end)) {
                    switch ($this->resv_rule) {
                        case RULE_PROTECTED:
                            $row_class = 'PROTECTED';
                            break;
                        case RULE_CLOSED:
                            $row_class = 'CLOSED';
                            break;
                        case RULE_FCFS:
                            $row_class = '';
                            break;
                    }
                }

                $html .="<TR class='$row_class'>";

                /* make use of our very clever rowspan query to only display the date
                  information one time.  If our new date does not equal the last date
                  then we are starting a new major row.  The number of rows that share
                  this date was calculated by rowspan.
                */
                if ($date != $last_date) {
                    $d = date('w-d',strtotime($day));
                    $html .= "<TD class='DATE' rowspan='$rowspan[$d]'>$date</TD>";
                }


                $utime = strtotime($day);
                $html .= "<TD class='TIME'>$time</TD>";

                /** This has to change in favor of check boxes. **/

                foreach ($locations as $key=>$value) {
                    // This is where the request logic needs to go.
					if ($value=="(void)") {
						$class = "BLACKEDOUT";
						$status = "---";
                        $slot_id = "";
					} else {
						$status = $value['team'];
						$class = $value['division'];
                        if ($status == 'OPEN') $class .= " OPEN";
                        if ($view_only)
                            $slot_info = "";
                        else {
                            $slot_info = "id='{$value['id']}' title='$date @ $time on Field $key' ";
                        }

                        if ($status == "RETURNED") $class="UNAVAIL";
					}

                    $html .= "<TD $slot_info class='{$class}'>{$status}</TD>";
                }
                $html .= "</TR>";
                $last_date = $date;
            }
            $html .="</table>";
            $html .="</div>";


            if ($view_only and $date) {
            // get usage statistics
                $total = $wpdb->get_var("SELECT count(*) FROM " . TSLL_RESERVATIONS_TABLE . " r " . $where);
                $remaining = $wpdb->get_var("SELECT count(*) FROM " . TSLL_RESERVATIONS_TABLE . " r " . $where . " AND team_id=". OPEN_TEAM);
                $used = $total - $remaining;
                $utilization = ($total > 0) ? $used / $total : 0.0;
                $utlization_str = sprintf("%2.1f",$utilization * 100.0);
                $html .= "<div><p>Field Utilization: $utlization_str%  [<small>$used / $total Fields reserved.</small>]</p></div>";
            }

            return $html;
        }

        /**
         * Darn near the heart of the whole thing.
         * This has new functionality.  It will display TWO weeks of fields
         * This week and next week if it is posting for a reservation window.
         *
         * If you give me a date, we'll show ONE week, with no date we show
         * the default of this week AND next week. (two weeks worth)
         *
         * Pass in dates in MYSQL or HUMAN format YYYY-MM-DD or MM/DD/YYYY
         * passing in a date gives you ONE week table
         * passing in NO date gives you this week AND next week (two weeks worth)
         */
        function generateReservationTable($view_only=false,$date=null)
        {
            global $wpdb;

            /**
             * Okay, handle paging through weeks of data.  This is a bit odd, but the fastest
             * way to do this is to put it here.  Let this app look for the week parameter
             * passed by the caller and display the appropriate buttons.
             */
            if ($date) {
                $window = $this->generateWeekWindow($date,DATE_MYSQL);
                $start = $window['Start'];
                $end = $window['End'];
            } else {
                $start = $this->sql_dates['ThisWeekStart'];
                $end = $this->sql_dates['NextWeekEnd'];
            }


            $where = " WHERE r.gamedate >= '$start' AND r.gamedate < '$end'";

            if ($wpdb->get_var("SELECT COUNT(*) FROM " . TSLL_RESERVATIONS_TABLE . " r " . $where) == 0)
                return "<div class='ERROR'>No Field Reservations Have Been Defined For Next Week.</div>";

            $sql = "SELECT r.*,f.name AS field_name, t.name as team_name, t.division FROM " . TSLL_RESERVATIONS_TABLE ." r "
                   . " INNER JOIN " . TSLL_TEAMS_TABLE . " t ON r.team_id = t.id "
                   . " INNER JOIN ". TSLL_FIELDS_TABLE . " f ON r.field_id = f.id "
                   . $where
                   . " ORDER BY r.gamedate";
            $slots = $wpdb->get_results($sql,ARRAY_A);

            // build the array

            $sql = "SELECT * FROM " . TSLL_FIELDS_TABLE . " ORDER BY priority,name ";
            $fields = $wpdb->get_results($sql,ARRAY_A);

            /**
             * Display Check.  Are there fields with NO slots available?
             */
            foreach ($fields as $f=>$field) {
                $sql = "SELECT COUNT(*) FROM " . TSLL_RESERVATIONS_TABLE . " r "
                . $where
                . " AND r.field_id=".$field['id'];
                $count = $wpdb->get_var($sql);
                if (empty($count)) {
                   unset($fields[$f]);
                }
            }

            /**
             * insane query to get rowspan
             * How this works -we need to do 2 queries, first we need to get all the dates
             * AND their times in one list (the subquery)
             * then we want to total the number of times per day - the outer query.
             * THAT is our row span.
             * So the inner query splits the gamedate object into two separate pieces date and time
             * the outer query tells us how many times per day we have.  Crazy.
            */
            /* TODO: this will need to be filtered by DATE at some point */
            $sql = "SELECT d,COUNT(*) AS rowspan FROM "
                    . " (SELECT DATE_FORMAT(gamedate,'%w-%d') d, DATE_FORMAT(gamedate,'%h:%i') AS t,COUNT(*) FROM " . TSLL_RESERVATIONS_TABLE . " r "
                    . " $where GROUP BY d,t ) AS tmp GROUP BY d";
            $rowspans = $wpdb->get_results($sql,ARRAY_A);
            $rowspan = array();
            // convert results to a simple lookup.
            foreach ($rowspans as $r) {
                $rowspan[$r['d']] = $r['rowspan'];
            }


            $matrix = array();
            // flesh out the array - remove duplicates for the gamedate.
            foreach ($slots as $slot) {
                $matrix[$slot['gamedate']] = null;
            }
            // now make sure every gamedate represents ALL fields.
            foreach ($matrix as $key=>$value) {
                foreach ($fields as $field)
                    $matrix[$key][$field['name']] = "open";
            }
            // Now display the assignements
            foreach ($slots as $slot) {
                $matrix[$slot['gamedate']][$slot['field_name']] = array('team'=>$slot['team_name'],'division'=>$slot['division'],'id'=>$slot['id']);
            }

            $html = "<div class='resv_panel'>";
            $html .= "<table id='resv_table' class='RESV_TABLE' border='1px solid black' cellspacing='0' cellpadding='2px'>";
            $html .= "<TR style='text-align: center;'>";
            $html .= "<th class='DATE_HEADER'>Date</th><th class='TIME_HEADER'>Time</th>";
            if ($view_only == false) $html .= "<th class='REQ_HEADER'>Req</th>";
            // generate the field count
            foreach ($fields as $field) {
                $html .= "<TH class='FIELD_HEADER'>{$field['name']}</TH>";
            }
            $html .= "</TR>";
            $last_date = null;
            $row_class = '';
            foreach ($matrix as $day=>$locations) {
                $uDay = strtotime($day);
                $date = date('D (m/d)',$uDay);
                $time = date('h:i A',$uDay);

                if ($view_only == false and ($uDay >= $this->next_week_start and $uDay <= $this->next_week_end)) {
                    switch ($this->resv_rule) {
                        case RULE_PROTECTED:
                            $row_class = 'PROTECTED';
                            break;
                        case RULE_CLOSED:
                            $row_class = 'CLOSED';
                            break;
                        case RULE_FCFS:
                            $row_class = '';
                            break;
                    }
                }

                $html .="<TR class='$row_class'>";
                if ($date != $last_date) {
                    $d = date('w-d',strtotime($day));
                    $html .= "<TD class='DATE' rowspan='$rowspan[$d]'>$date</TD>";
                }
                $utime = strtotime($day);
                $html .= "<TD class='TIME'>$time</TD>";
                if ($view_only == false) {
                    if ($row_class == 'CLOSED')
                        $html .="<TD id='$utime' class='CLOSED'>NA</TD>";
                    else
                        $html .="<TD id='$utime' class='OPEN'><input type='checkbox'/></TD>";
                }



                foreach ($locations as $key=>$value) {
					if ($value=="open") {
						$class = "BLACKEDOUT";
						$status = "---";
					} else {
						$status = $value['team'];
						$class = $value['division'];
                        if ($status == "RETURNED") $class="UNAVAIL";
					}
                    $html .= "<TD class='{$class}'>{$status}</TD>";
                }
                $html .= "</TR>";
                $last_date = $date;
            }
            $html .="</table>";
            $html .="</div>";


            if ($view_only and $date) {
            // get usage statistics
                $total = $wpdb->get_var("SELECT count(*) FROM " . TSLL_RESERVATIONS_TABLE . " r " . $where);
                $remaining = $wpdb->get_var("SELECT count(*) FROM " . TSLL_RESERVATIONS_TABLE . " r " . $where . " AND team_id=". OPEN_TEAM);
                $used = $total - $remaining;
                $utilization = ($total > 0) ? $used / $total : 0.0;
                $utlization_str = sprintf("%2.1f",$utilization * 100.0);
                $html .= "<div><p>Field Utilization: $utlization_str%  [<small>$used / $total Fields reserved.</small>]</p></div>";
            }

            return $html;
        }

        function grantCount($uRequested,$team_id,&$denied) {
            global $wpdb;

            // they are requesting an area that's FCFS so grant a field.
            if ($uRequested >= $this->this_week_start && $uRequested <= $this->this_week_end) {
                $thisWeek = true;
                $start = $this->sql_dates['ThisWeekStart'];
                $end = $this->sql_dates['ThisWeekEnd'];
            } else {
                $thisWeek = false;
                $start = $this->sql_dates['NextWeekStart'];
                $end = $this->sql_dates['NextWeekEnd'];
            }

            $sql = "SELECT count(*) FROM " . TSLL_RESERVATIONS_TABLE . " WHERE team_id=$team_id
                    AND DATE(gamedate) >= '$start' AND DATE(gamedate) < '$end'";

            $reservation_count = $wpdb->get_var($sql);
            if ($thisWeek) {
                $weekStr = $this->human_dates['ThisWeek'];
            } else {
                $weekStr = $this->human_dates['NextWeek'];
            }
            $denied = "Unable to allocate you another field.<br/>You already have $reservation_count fields reserved for the week of $weekStr.";

            return $reservation_count;
        }

        /**
         * Handle the front end user requests
         * This is called by an Ajax call.
         * It handles ALL the requests from the front end reservation system.
         *
         * StartRequest - generates new tables.
         */
        function fieldRequest()
        {
            global $wpdb;
            global $current_user;
            get_currentuserinfo();

            $action = $this->getParam('tsll_action');
            $response = array(success=>false,html=>"");

            switch ($action) {
                case 'StartRequest':
                    $sql = "SELECT * FROM " . TSLL_TEAMS_TABLE . " WHERE email='{$current_user->user_email}'";
                    $team = $wpdb->get_row($sql,ARRAY_A);

                    $response['success'] = true;
                    $response['html'] = "<strong>". $team['manager'].  "</strong> "
                            ."logged in for the "
                            . "<strong id='team_name'>".$team['name']."</strong> ";
                    $response['table'] = $this->generateNewReservationTable();
                    $response['team_id'] = $team['id'];
                    break;

                case 'request':
                    /**
                     * This is pretty tricky stuff.  I need to work on a test case for all situations.
                     */
                    $request = $this->getParam('request');
                    if (empty($request)) {
                        $response['success'] = false;
                        $response['html'] = "Need at least <b>ONE</b> Practice Time Slot";
                        $response['targetURL'] = get_option('siteurl') . "/reservations/";
                    } else {
                        $team_id = $this->getParam('team_id');
                        $slot_id = $request;

                        $sql = "SELECT r.id,
                                r.gamedate,
                                r.field_id,
                                r.team_id,
                                f.name AS field_name
                                FROM  " . TSLL_RESERVATIONS_TABLE ."  r
                                INNER JOIN ". TSLL_FIELDS_TABLE ." f ON (r.field_id=f.id)
                                WHERE r.id=$slot_id";

                        $slot = $wpdb->get_row($sql,ARRAY_A);

                        $sql = "SELECT * FROM ". TSLL_TEAMS_TABLE . " WHERE id=".$team_id;

                        $team = $wpdb->get_row($sql,ARRAY_A);

                        if ($slot['team_id'] != 1) {
                            $response['success'] = false;
                            $response['html'] = "<p><strong>Oh No!</strong> Someone beat you to it!  That field has already been reserved.</p><p>Reload the Reservation Tool and please try again.</p>";
                            $response['targetURL'] = get_option('siteurl') . "/reservations/";
                            die(json_encode($response));
                        }

                        $req_timestamp = time();
                        $response['success'] = true;
                        $response['html'] = "<p>Field request  for <strong>" . $team['name'] . " (" . ucwords( strtolower($team['division'])) . ") </strong></p>"
                            . "<p>Has been logged at <b>" .date('h:i:s A D, M/d/Y',$req_timestamp) . "</b></p> "
                            . "<p>You will receive an email when your request has been processed.</p>";
                        $response['targetURL'] = get_option('siteurl') . "/reservations/";


                        $created = date('Y-m-d H:i:00',$req_timestamp);


                        // is second week protected?

                        $uRequested = strtotime($slot['gamedate']);
                        $reject_message = "Can Not Allocate Field";
						if ($this->resv_rule == RULE_PROTECTED) {
							// yes.  Team can only get one field.
							if ($uRequested >= $this->next_week_start and $uRequested<= $this->next_week_end) {
								// they can only have one request in this window.
								$start = $this->sql_dates['NextWeekStart'];
								$end = $this->sql_dates['NextWeekEnd'];
								$sql = "SELECT count(*) FROM " . TSLL_RESERVATIONS_TABLE . " WHERE team_id=$team_id
										AND DATE(gamedate) >= '$start' AND DATE(gamedate) < '$end'";
								$reservation_count = $wpdb->get_var($sql);
								if ($reservation_count>0) {
									$response['html'] = "Unable to allocate you another field.  You already have $reservation_count field(s) "
                                            . " reserved.  Next week is not yet on a <b>First Come First Serve</b> basis.";
									$make_grant = false;
								} else {
									// they don't have any fields grant their request.
									$make_grant = true;
								}
							} else {
                                $reservation_count = $this->grantCount($uRequested,$team_id,$reject_message);
                                if ($reservation_count>3) {
                                    $response['html'] = $reject_message;
                                    $make_grant = false;
                                } else {
                                    //  grant their request.
                                    $make_grant = true;
                                }
							}
						} else if ($this->resv_rule == RULE_CLOSED) {
							if ($uRequested >= $this->next_week_start and $uRequested <= $this->next_week_end) {
								$response['html'] = "Unable to allocate field.  Next week is blocked off.  System will
										begin accepting reservations: " . $this->protect_dates['Start_Label'];
								$make_grant = false;
							} else {
								// they are requesting an area that's FCFS so grant a field.
                                $reservation_count = $this->grantCount($uRequested,$team_id,$reject_message);
                                if ($reservation_count>3) {
                                    $response['html'] = $reject_message;
                                    $make_grant = false;
                                } else {
                                    //  grant their request.
                                    $make_grant = true;
                                }
                            }
						} else {
                            $reservation_count = $this->grantCount($uRequested,$team_id,$reject_message);
                            if ($reservation_count>3) {
                                $response['html'] = $reject_message;
                                $make_grant = false;
                            } else {
                                //  grant their request.
                                $make_grant = true;
                            }
						}

						if ($make_grant) {

							$wpdb->update(TSLL_RESERVATIONS_TABLE, array('team_id'=>$team_id,'modified'=>date('Y-m-d H:i:s')), array('id'=>$slot_id)); // reserve the field.

                            // make this more user friendly in the future.
                            $resv = $wpdb->get_row("SELECT *,DATE_FORMAT(gamedate,'%a %b-%d At %h:%i %p') as resv_date FROM " . TSLL_RESERVATIONS_TABLE . " WHERE id=$slot_id",ARRAY_A);
                            $team = $wpdb->get_row("SELECT * FROM " . TSLL_TEAMS_TABLE . " WHERE id=".$resv['team_id'],ARRAY_A);
                            $field = $wpdb->get_row("SELECT * FROM " . TSLL_FIELDS_TABLE . " WHERE id=".$resv['field_id'],ARRAY_A);
                            $code = substr(md5($slot_id),0,8);  // release code
                            $path = get_bloginfo('url') . "/release-reservation?reservation_id=$slot_id&release_code=$code";
                            $response['html'] = "<p>The System has allocated you a field based upon your request.</p>";
                            $response['html'] .="<p>Field " . $field['name'] . " is reserved on ".$resv['resv_date']. " for the " . $team['name'] . "</p>";
                            $response['html'] .="<p>Your reservation number is: $slot_id and your release code is: $code<p>";

                            // God I hope this works!
                            $to = $current_user->user_email;
                            $subject ="Field Request Granted";
                            $body = "The Tempe South Baseball System has allocated a field based upon your request.\n";
                            $body .="Field " . $field['name'] . " is reserved on ".$resv['resv_date']. " for the " . $team['name'] . "\n\n\n";
                            $body .="To release this reservation please visit $path". "\n\n";
                            $body .="Your reservation id is: $slot_id and your release code is: $code " . "\n";
                            $body .="Thank you.";

                            $headers =  'From: webmaster@tempesouth.com' . "\r\n" .
                                        'Reply-To: webmaster@tempesouth.com' . "\r\n" .
                                        'X-Mailer: PHP/' . phpversion();
                            mail($to,$subject,$body,$headers);
						} else {
                            $response['success'] = false;
                        }
					}
                    break;

                default:
                    $response['success'] = false;
                    $response['html'] = "Invalid Request.  Please try again.";
            }
            die(json_encode($response));
        }
    }

} // End Class Constructor

/**
 * AJAX Functions
 *
 * These can't be inside an object - because the object might not be instantiated.
 */

/**
 * tsll_field_request
 * User requests a field
 * this is the core ajax call from Reservation system.
 */
function tsll_field_request()
{
    global $tsllfields;

    $tsllfields->fieldRequest();

}


/**
 * Instantiate the class if class has been delcared
 */
if (class_exists("tsllfields")) {
	$tsllfields = new tsllfields();
}

// Hook the Actions and Filters
if (isset($tsllfields)) {

    // Actions
    add_action('admin_menu',array(&$tsllfields, 'addResvMenu'));
	add_action('wp_head',array(&$tsllfields, 'addHeaderCode'));
	add_action('init',array(&$tsllfields, 'headerInit'));
    add_action('admin_head',array(&$tsllfields, 'adminHeaders'));

    // AJAX actions

    add_action('wp_ajax_tsll_field_request','tsll_field_request');

	// Filters
	add_filter('the_content',array(&$tsllfields, 'addContent'));

}

/**
 * Handle when the plugin is deactivated.
 */

function tsll_deactivate() {
	// For now wipe the databases.
	global $wpdb;

	$wpdb->query("DROP TABLE IF EXISTS " . TSLL_FIELDS_TABLE);
	$wpdb->query("DROP TABLE IF EXISTS " . TSLL_GAMETIMES_TABLE);
	$wpdb->query("DROP TABLE IF EXISTS " . TSLL_OPTIONS_TABLE);
	$wpdb->query("DROP TABLE IF EXISTS " . TSLL_REQUESTS_TABLE);
	$wpdb->query("DROP TABLE IF EXISTS " . TSLL_RESERVATIONS_TABLE);
	$wpdb->query("DROP TABLE IF EXISTS " . TSLL_TEAMS_TABLE);

}
register_deactivation_hook( __FILE__, 'tsll_deactivate' );


?>
